﻿namespace Comercializadora
{
    partial class LavaSamsung
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ComprarLavaSamsung3 = new System.Windows.Forms.Button();
            this.AgregarLavaSamsung3 = new System.Windows.Forms.Button();
            this.AgregarLavaSamsung2 = new System.Windows.Forms.Button();
            this.ComprarLavaSamsung2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarLavaSamsung1 = new System.Windows.Forms.Button();
            this.ComprarLavaSamsung1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.LavaSamsung2;
            this.pictureBox4.Location = new System.Drawing.Point(12, 344);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(154, 211);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.LavaSamsung3_1;
            this.pictureBox3.Location = new System.Drawing.Point(12, 573);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(154, 211);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.LavaSamsung1;
            this.pictureBox2.Location = new System.Drawing.Point(12, 116);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(154, 211);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.samsung1;
            this.pictureBox1.Location = new System.Drawing.Point(286, 41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ComprarLavaSamsung3
            // 
            this.ComprarLavaSamsung3.Location = new System.Drawing.Point(200, 767);
            this.ComprarLavaSamsung3.Name = "ComprarLavaSamsung3";
            this.ComprarLavaSamsung3.Size = new System.Drawing.Size(105, 23);
            this.ComprarLavaSamsung3.TabIndex = 41;
            this.ComprarLavaSamsung3.Text = "Comprar";
            this.ComprarLavaSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarLavaSamsung3
            // 
            this.AgregarLavaSamsung3.Location = new System.Drawing.Point(340, 767);
            this.AgregarLavaSamsung3.Name = "AgregarLavaSamsung3";
            this.AgregarLavaSamsung3.Size = new System.Drawing.Size(105, 23);
            this.AgregarLavaSamsung3.TabIndex = 40;
            this.AgregarLavaSamsung3.Text = "Agregar al carrito";
            this.AgregarLavaSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarLavaSamsung2
            // 
            this.AgregarLavaSamsung2.Location = new System.Drawing.Point(340, 532);
            this.AgregarLavaSamsung2.Name = "AgregarLavaSamsung2";
            this.AgregarLavaSamsung2.Size = new System.Drawing.Size(105, 23);
            this.AgregarLavaSamsung2.TabIndex = 39;
            this.AgregarLavaSamsung2.Text = "Agregar al carrito";
            this.AgregarLavaSamsung2.UseVisualStyleBackColor = true;
            // 
            // ComprarLavaSamsung2
            // 
            this.ComprarLavaSamsung2.Location = new System.Drawing.Point(202, 532);
            this.ComprarLavaSamsung2.Name = "ComprarLavaSamsung2";
            this.ComprarLavaSamsung2.Size = new System.Drawing.Size(105, 23);
            this.ComprarLavaSamsung2.TabIndex = 38;
            this.ComprarLavaSamsung2.Text = "Comprar";
            this.ComprarLavaSamsung2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(200, 696);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(206, 39);
            this.label12.TabIndex = 37;
            this.label12.Text = "Lavadora automatica 19 Kg\r\n Color Plata con negro y detalles en cromo\r\nPanel digi" +
    "tal";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(203, 670);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 36;
            this.label11.Text = "$ 9,965.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(203, 641);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 15);
            this.label10.TabIndex = 35;
            this.label10.Text = "Marca: SAMSUNG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(200, 588);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(380, 44);
            this.label9.TabIndex = 34;
            this.label9.Text = "LAVADORA 19KG AUTOMÁTICA NEGRO \r\nCON DETALLES CROMADOS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(200, 465);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 39);
            this.label8.TabIndex = 33;
            this.label8.Text = "11 ciclos de lavado\r\n10 niveles de agua\r\nTina con acabado diamante";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(203, 433);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 15);
            this.label7.TabIndex = 32;
            this.label7.Text = " 9,999.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(200, 402);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "Marca: SAMSUNG";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(199, 368);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(318, 22);
            this.label5.TabIndex = 30;
            this.label5.Text = "Lavadora Samsung 19 Kg Blanca";
            // 
            // AgregarLavaSamsung1
            // 
            this.AgregarLavaSamsung1.Location = new System.Drawing.Point(340, 304);
            this.AgregarLavaSamsung1.Name = "AgregarLavaSamsung1";
            this.AgregarLavaSamsung1.Size = new System.Drawing.Size(105, 23);
            this.AgregarLavaSamsung1.TabIndex = 29;
            this.AgregarLavaSamsung1.Text = "Agregar al carrito";
            this.AgregarLavaSamsung1.UseVisualStyleBackColor = true;
            // 
            // ComprarLavaSamsung1
            // 
            this.ComprarLavaSamsung1.Location = new System.Drawing.Point(200, 304);
            this.ComprarLavaSamsung1.Name = "ComprarLavaSamsung1";
            this.ComprarLavaSamsung1.Size = new System.Drawing.Size(105, 23);
            this.ComprarLavaSamsung1.TabIndex = 28;
            this.ComprarLavaSamsung1.Text = "Comprar";
            this.ComprarLavaSamsung1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(197, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(240, 52);
            this.label4.TabIndex = 27;
            this.label4.Text = "Recomendada para 4 personas\r\nManual de programación de lavado\r\nCarga superior\r\nSe" +
    " recomienda no interrumpir los ciclos de lavado";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(200, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 26;
            this.label3.Text = "$ 8,899.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(200, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 15);
            this.label2.TabIndex = 25;
            this.label2.Text = "Marca: SAMSUNG";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(196, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(465, 22);
            this.label1.TabIndex = 24;
            this.label1.Text = "Lavadora Samsung Carga Superior 17 Kg Blanca";
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(-1, -3);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 142;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // LavaSamsung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(805, 793);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.ComprarLavaSamsung3);
            this.Controls.Add(this.AgregarLavaSamsung3);
            this.Controls.Add(this.AgregarLavaSamsung2);
            this.Controls.Add(this.ComprarLavaSamsung2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarLavaSamsung1);
            this.Controls.Add(this.ComprarLavaSamsung1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "LavaSamsung";
            this.Text = "LavaSamsung";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button ComprarLavaSamsung3;
        private System.Windows.Forms.Button AgregarLavaSamsung3;
        private System.Windows.Forms.Button AgregarLavaSamsung2;
        private System.Windows.Forms.Button ComprarLavaSamsung2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarLavaSamsung1;
        private System.Windows.Forms.Button ComprarLavaSamsung1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}