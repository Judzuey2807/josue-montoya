﻿namespace Comercializadora
{
    partial class MicroMabe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MicroMabe));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ComprarMicroMabe3 = new System.Windows.Forms.Button();
            this.AgregarMicroMabe3 = new System.Windows.Forms.Button();
            this.AgregarMicroMabe2 = new System.Windows.Forms.Button();
            this.ComprarMicroMabe2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarMicroMabe1 = new System.Windows.Forms.Button();
            this.ComprarMicroMabe1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.MicroMabe3;
            this.pictureBox4.Location = new System.Drawing.Point(12, 515);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(229, 139);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.MicroMabe2;
            this.pictureBox3.Location = new System.Drawing.Point(12, 329);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(220, 129);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.MicroMabe1;
            this.pictureBox2.Location = new System.Drawing.Point(12, 133);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(220, 130);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.Mabe;
            this.pictureBox1.Location = new System.Drawing.Point(285, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(226, 87);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // ComprarMicroMabe3
            // 
            this.ComprarMicroMabe3.Location = new System.Drawing.Point(260, 687);
            this.ComprarMicroMabe3.Name = "ComprarMicroMabe3";
            this.ComprarMicroMabe3.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicroMabe3.TabIndex = 95;
            this.ComprarMicroMabe3.Text = "Comprar";
            this.ComprarMicroMabe3.UseVisualStyleBackColor = true;
            // 
            // AgregarMicroMabe3
            // 
            this.AgregarMicroMabe3.Location = new System.Drawing.Point(398, 687);
            this.AgregarMicroMabe3.Name = "AgregarMicroMabe3";
            this.AgregarMicroMabe3.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicroMabe3.TabIndex = 94;
            this.AgregarMicroMabe3.Text = "Agregar al carrito";
            this.AgregarMicroMabe3.UseVisualStyleBackColor = true;
            // 
            // AgregarMicroMabe2
            // 
            this.AgregarMicroMabe2.Location = new System.Drawing.Point(398, 456);
            this.AgregarMicroMabe2.Name = "AgregarMicroMabe2";
            this.AgregarMicroMabe2.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicroMabe2.TabIndex = 93;
            this.AgregarMicroMabe2.Text = "Agregar al carrito";
            this.AgregarMicroMabe2.UseVisualStyleBackColor = true;
            // 
            // ComprarMicroMabe2
            // 
            this.ComprarMicroMabe2.Location = new System.Drawing.Point(260, 456);
            this.ComprarMicroMabe2.Name = "ComprarMicroMabe2";
            this.ComprarMicroMabe2.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicroMabe2.TabIndex = 92;
            this.ComprarMicroMabe2.Text = "Comprar";
            this.ComprarMicroMabe2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(267, 588);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(386, 91);
            this.label12.TabIndex = 91;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(257, 563);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 15);
            this.label11.TabIndex = 90;
            this.label11.Text = " 1,743.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(256, 537);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 15);
            this.label10.TabIndex = 89;
            this.label10.Text = "Marca: MABE";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(256, 515);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(476, 22);
            this.label9.TabIndex = 88;
            this.label9.Text = "Horno Microondas Mabe 0.7 Pies Mabe HMM70SW";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(267, 398);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(171, 39);
            this.label8.TabIndex = 87;
            this.label8.Text = "10 niveles de potencia\r\n6 selecciones rápidas de cocinado\r\nSeguro para niños";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(267, 383);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 15);
            this.label7.TabIndex = 86;
            this.label7.Text = "$ 2,900.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(267, 358);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 85;
            this.label6.Text = "Marca: MABE";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(266, 314);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(309, 44);
            this.label5.TabIndex = 84;
            this.label5.Text = "HORNO DE MICROONDAS 1.4 P3\r\nCUFT ESPEJO PLATA Mabe ";
            // 
            // AgregarMicroMabe1
            // 
            this.AgregarMicroMabe1.Location = new System.Drawing.Point(398, 251);
            this.AgregarMicroMabe1.Name = "AgregarMicroMabe1";
            this.AgregarMicroMabe1.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicroMabe1.TabIndex = 83;
            this.AgregarMicroMabe1.Text = "Agregar al carrito";
            this.AgregarMicroMabe1.UseVisualStyleBackColor = true;
            // 
            // ComprarMicroMabe1
            // 
            this.ComprarMicroMabe1.Location = new System.Drawing.Point(259, 251);
            this.ComprarMicroMabe1.Name = "ComprarMicroMabe1";
            this.ComprarMicroMabe1.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicroMabe1.TabIndex = 82;
            this.ComprarMicroMabe1.Text = "Comprar";
            this.ComprarMicroMabe1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(267, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 39);
            this.label4.TabIndex = 81;
            this.label4.Text = "Fácil Limpieza\r\nTamaño: 26.2 x 45.2 x 33\r\nCon plato giratorio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(267, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 15);
            this.label3.TabIndex = 80;
            this.label3.Text = "$ 990.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(267, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 79;
            this.label2.Text = "Marca: MABE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(266, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(496, 22);
            this.label1.TabIndex = 78;
            this.label1.Text = "Horno de Microondas Mabe 0.7 Pies Cúbicos Blanco";
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(-3, 0);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 141;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // MicroMabe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(828, 722);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.ComprarMicroMabe3);
            this.Controls.Add(this.AgregarMicroMabe3);
            this.Controls.Add(this.AgregarMicroMabe2);
            this.Controls.Add(this.ComprarMicroMabe2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarMicroMabe1);
            this.Controls.Add(this.ComprarMicroMabe1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "MicroMabe";
            this.Text = "MicroMabe";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button ComprarMicroMabe3;
        private System.Windows.Forms.Button AgregarMicroMabe3;
        private System.Windows.Forms.Button AgregarMicroMabe2;
        private System.Windows.Forms.Button ComprarMicroMabe2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarMicroMabe1;
        private System.Windows.Forms.Button ComprarMicroMabe1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}