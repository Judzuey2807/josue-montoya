﻿namespace Comercializadora
{
    partial class LineaBlanca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtVerTodoAire = new System.Windows.Forms.Button();
            this.BtVerTodoRefri = new System.Windows.Forms.Button();
            this.BtVerTodoLava = new System.Windows.Forms.Button();
            this.BtVerTodoMicro = new System.Windows.Forms.Button();
            this.BtRefriSamsung = new System.Windows.Forms.Button();
            this.BtRefriWhirlpool = new System.Windows.Forms.Button();
            this.BtRefriLg = new System.Windows.Forms.Button();
            this.BtRefriMabe = new System.Windows.Forms.Button();
            this.BtLavaSamsung = new System.Windows.Forms.Button();
            this.BtLavaWhirlpool = new System.Windows.Forms.Button();
            this.BtLavaGeneral = new System.Windows.Forms.Button();
            this.BtLavaLg = new System.Windows.Forms.Button();
            this.BtMicroSamsung = new System.Windows.Forms.Button();
            this.BtMicroWhirlpool = new System.Windows.Forms.Button();
            this.BtMicroMabe = new System.Windows.Forms.Button();
            this.BtMicroDaewoo = new System.Windows.Forms.Button();
            this.BtAireSamsung = new System.Windows.Forms.Button();
            this.BtAireLg = new System.Windows.Forms.Button();
            this.BtAireCarrier = new System.Windows.Forms.Button();
            this.BtAireMabe = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btAire = new System.Windows.Forms.Button();
            this.btMicro = new System.Windows.Forms.Button();
            this.btLavadora = new System.Windows.Forms.Button();
            this.btRefrigerador = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BtVerTodoAire
            // 
            this.BtVerTodoAire.Location = new System.Drawing.Point(644, 437);
            this.BtVerTodoAire.Name = "BtVerTodoAire";
            this.BtVerTodoAire.Size = new System.Drawing.Size(84, 23);
            this.BtVerTodoAire.TabIndex = 44;
            this.BtVerTodoAire.Text = "Ver todo";
            this.BtVerTodoAire.UseVisualStyleBackColor = true;
            this.BtVerTodoAire.Click += new System.EventHandler(this.bVerTodo_Click);
            // 
            // BtVerTodoRefri
            // 
            this.BtVerTodoRefri.Location = new System.Drawing.Point(31, 437);
            this.BtVerTodoRefri.Name = "BtVerTodoRefri";
            this.BtVerTodoRefri.Size = new System.Drawing.Size(84, 23);
            this.BtVerTodoRefri.TabIndex = 48;
            this.BtVerTodoRefri.Text = "Ver Todos";
            this.BtVerTodoRefri.UseVisualStyleBackColor = true;
            this.BtVerTodoRefri.Click += new System.EventHandler(this.BtAceptarRefri_Click);
            // 
            // BtVerTodoLava
            // 
            this.BtVerTodoLava.Location = new System.Drawing.Point(226, 437);
            this.BtVerTodoLava.Name = "BtVerTodoLava";
            this.BtVerTodoLava.Size = new System.Drawing.Size(84, 23);
            this.BtVerTodoLava.TabIndex = 49;
            this.BtVerTodoLava.Text = "Ver todos";
            this.BtVerTodoLava.UseVisualStyleBackColor = true;
            this.BtVerTodoLava.Click += new System.EventHandler(this.btAceptarLava_Click);
            // 
            // BtVerTodoMicro
            // 
            this.BtVerTodoMicro.Location = new System.Drawing.Point(443, 437);
            this.BtVerTodoMicro.Name = "BtVerTodoMicro";
            this.BtVerTodoMicro.Size = new System.Drawing.Size(84, 23);
            this.BtVerTodoMicro.TabIndex = 50;
            this.BtVerTodoMicro.Text = "Ver todos";
            this.BtVerTodoMicro.UseVisualStyleBackColor = true;
            this.BtVerTodoMicro.Click += new System.EventHandler(this.BtVerTodoMicro_Click);
            // 
            // BtRefriSamsung
            // 
            this.BtRefriSamsung.Location = new System.Drawing.Point(31, 327);
            this.BtRefriSamsung.Name = "BtRefriSamsung";
            this.BtRefriSamsung.Size = new System.Drawing.Size(84, 23);
            this.BtRefriSamsung.TabIndex = 68;
            this.BtRefriSamsung.Text = "SAMSUNG";
            this.BtRefriSamsung.UseVisualStyleBackColor = true;
            this.BtRefriSamsung.Click += new System.EventHandler(this.BtRefriSamsung_Click);
            // 
            // BtRefriWhirlpool
            // 
            this.BtRefriWhirlpool.Location = new System.Drawing.Point(31, 408);
            this.BtRefriWhirlpool.Name = "BtRefriWhirlpool";
            this.BtRefriWhirlpool.Size = new System.Drawing.Size(84, 23);
            this.BtRefriWhirlpool.TabIndex = 69;
            this.BtRefriWhirlpool.Text = "WHIRLPOOL";
            this.BtRefriWhirlpool.UseVisualStyleBackColor = true;
            this.BtRefriWhirlpool.Click += new System.EventHandler(this.BtRefriWhirlpool_Click);
            // 
            // BtRefriLg
            // 
            this.BtRefriLg.Location = new System.Drawing.Point(31, 381);
            this.BtRefriLg.Name = "BtRefriLg";
            this.BtRefriLg.Size = new System.Drawing.Size(84, 23);
            this.BtRefriLg.TabIndex = 70;
            this.BtRefriLg.Text = "LG";
            this.BtRefriLg.UseVisualStyleBackColor = true;
            this.BtRefriLg.Click += new System.EventHandler(this.BtRefriLg_Click);
            // 
            // BtRefriMabe
            // 
            this.BtRefriMabe.Location = new System.Drawing.Point(31, 352);
            this.BtRefriMabe.Name = "BtRefriMabe";
            this.BtRefriMabe.Size = new System.Drawing.Size(84, 23);
            this.BtRefriMabe.TabIndex = 71;
            this.BtRefriMabe.Text = "MABE";
            this.BtRefriMabe.UseVisualStyleBackColor = true;
            this.BtRefriMabe.Click += new System.EventHandler(this.BtRefriMabe_Click);
            // 
            // BtLavaSamsung
            // 
            this.BtLavaSamsung.Location = new System.Drawing.Point(226, 327);
            this.BtLavaSamsung.Name = "BtLavaSamsung";
            this.BtLavaSamsung.Size = new System.Drawing.Size(84, 23);
            this.BtLavaSamsung.TabIndex = 72;
            this.BtLavaSamsung.Text = "SAMSUNG";
            this.BtLavaSamsung.UseVisualStyleBackColor = true;
            this.BtLavaSamsung.Click += new System.EventHandler(this.BtLavaSamsung_Click);
            // 
            // BtLavaWhirlpool
            // 
            this.BtLavaWhirlpool.Location = new System.Drawing.Point(226, 352);
            this.BtLavaWhirlpool.Name = "BtLavaWhirlpool";
            this.BtLavaWhirlpool.Size = new System.Drawing.Size(84, 23);
            this.BtLavaWhirlpool.TabIndex = 73;
            this.BtLavaWhirlpool.Text = "WHIRLPOOL";
            this.BtLavaWhirlpool.UseVisualStyleBackColor = true;
            this.BtLavaWhirlpool.Click += new System.EventHandler(this.BtLavaWhirlpool_Click);
            // 
            // BtLavaGeneral
            // 
            this.BtLavaGeneral.Location = new System.Drawing.Point(226, 408);
            this.BtLavaGeneral.Name = "BtLavaGeneral";
            this.BtLavaGeneral.Size = new System.Drawing.Size(84, 23);
            this.BtLavaGeneral.TabIndex = 75;
            this.BtLavaGeneral.Text = "GEBERAL ELECTRIC";
            this.BtLavaGeneral.UseVisualStyleBackColor = true;
            this.BtLavaGeneral.Click += new System.EventHandler(this.BtLavaGeneral_Click);
            // 
            // BtLavaLg
            // 
            this.BtLavaLg.Location = new System.Drawing.Point(226, 381);
            this.BtLavaLg.Name = "BtLavaLg";
            this.BtLavaLg.Size = new System.Drawing.Size(84, 23);
            this.BtLavaLg.TabIndex = 76;
            this.BtLavaLg.Text = "LG";
            this.BtLavaLg.UseVisualStyleBackColor = true;
            this.BtLavaLg.Click += new System.EventHandler(this.BtLavaLg_Click);
            // 
            // BtMicroSamsung
            // 
            this.BtMicroSamsung.Location = new System.Drawing.Point(443, 327);
            this.BtMicroSamsung.Name = "BtMicroSamsung";
            this.BtMicroSamsung.Size = new System.Drawing.Size(84, 23);
            this.BtMicroSamsung.TabIndex = 77;
            this.BtMicroSamsung.Text = "SAMSUNG";
            this.BtMicroSamsung.UseVisualStyleBackColor = true;
            this.BtMicroSamsung.Click += new System.EventHandler(this.BtMicroSamsung_Click);
            // 
            // BtMicroWhirlpool
            // 
            this.BtMicroWhirlpool.Location = new System.Drawing.Point(443, 352);
            this.BtMicroWhirlpool.Name = "BtMicroWhirlpool";
            this.BtMicroWhirlpool.Size = new System.Drawing.Size(84, 23);
            this.BtMicroWhirlpool.TabIndex = 78;
            this.BtMicroWhirlpool.Text = "WHIRLPOOL";
            this.BtMicroWhirlpool.UseVisualStyleBackColor = true;
            this.BtMicroWhirlpool.Click += new System.EventHandler(this.BtMicroWhirlpool_Click);
            // 
            // BtMicroMabe
            // 
            this.BtMicroMabe.Location = new System.Drawing.Point(443, 381);
            this.BtMicroMabe.Name = "BtMicroMabe";
            this.BtMicroMabe.Size = new System.Drawing.Size(84, 23);
            this.BtMicroMabe.TabIndex = 79;
            this.BtMicroMabe.Text = "MABE";
            this.BtMicroMabe.UseVisualStyleBackColor = true;
            this.BtMicroMabe.Click += new System.EventHandler(this.BtMicroMabe_Click);
            // 
            // BtMicroDaewoo
            // 
            this.BtMicroDaewoo.Location = new System.Drawing.Point(443, 408);
            this.BtMicroDaewoo.Name = "BtMicroDaewoo";
            this.BtMicroDaewoo.Size = new System.Drawing.Size(84, 23);
            this.BtMicroDaewoo.TabIndex = 80;
            this.BtMicroDaewoo.Text = "DAEWOO";
            this.BtMicroDaewoo.UseVisualStyleBackColor = true;
            this.BtMicroDaewoo.Click += new System.EventHandler(this.BtMicroDaewoo_Click);
            // 
            // BtAireSamsung
            // 
            this.BtAireSamsung.Location = new System.Drawing.Point(644, 327);
            this.BtAireSamsung.Name = "BtAireSamsung";
            this.BtAireSamsung.Size = new System.Drawing.Size(84, 23);
            this.BtAireSamsung.TabIndex = 81;
            this.BtAireSamsung.Text = "SAMSUNG";
            this.BtAireSamsung.UseVisualStyleBackColor = true;
            this.BtAireSamsung.Click += new System.EventHandler(this.button5_Click);
            // 
            // BtAireLg
            // 
            this.BtAireLg.Location = new System.Drawing.Point(644, 352);
            this.BtAireLg.Name = "BtAireLg";
            this.BtAireLg.Size = new System.Drawing.Size(84, 23);
            this.BtAireLg.TabIndex = 82;
            this.BtAireLg.Text = "LG";
            this.BtAireLg.UseVisualStyleBackColor = true;
            this.BtAireLg.Click += new System.EventHandler(this.BtAireLg_Click);
            // 
            // BtAireCarrier
            // 
            this.BtAireCarrier.Location = new System.Drawing.Point(644, 408);
            this.BtAireCarrier.Name = "BtAireCarrier";
            this.BtAireCarrier.Size = new System.Drawing.Size(84, 23);
            this.BtAireCarrier.TabIndex = 83;
            this.BtAireCarrier.Text = "CARRIER";
            this.BtAireCarrier.UseVisualStyleBackColor = true;
            this.BtAireCarrier.Click += new System.EventHandler(this.BtAireCarrier_Click);
            // 
            // BtAireMabe
            // 
            this.BtAireMabe.Location = new System.Drawing.Point(644, 381);
            this.BtAireMabe.Name = "BtAireMabe";
            this.BtAireMabe.Size = new System.Drawing.Size(84, 23);
            this.BtAireMabe.TabIndex = 84;
            this.BtAireMabe.Text = "MABE";
            this.BtAireMabe.UseVisualStyleBackColor = true;
            this.BtAireMabe.Click += new System.EventHandler(this.BtAireMabe_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(545, 476);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(183, 23);
            this.button1.TabIndex = 85;
            this.button1.Text = "Ver todos los productos";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.LineaBlanca;
            this.pictureBox1.Location = new System.Drawing.Point(0, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(565, 188);
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            // 
            // btAire
            // 
            this.btAire.Image = global::Comercializadora.Properties.Resources.Aire_acondicionado;
            this.btAire.Location = new System.Drawing.Point(635, 236);
            this.btAire.Name = "btAire";
            this.btAire.Size = new System.Drawing.Size(101, 76);
            this.btAire.TabIndex = 37;
            this.btAire.UseVisualStyleBackColor = true;
            this.btAire.Click += new System.EventHandler(this.btAire_Click);
            // 
            // btMicro
            // 
            this.btMicro.Image = global::Comercializadora.Properties.Resources.Microondas;
            this.btMicro.Location = new System.Drawing.Point(423, 232);
            this.btMicro.Name = "btMicro";
            this.btMicro.Size = new System.Drawing.Size(132, 85);
            this.btMicro.TabIndex = 36;
            this.btMicro.UseVisualStyleBackColor = true;
            this.btMicro.Click += new System.EventHandler(this.btMicro_Click);
            // 
            // btLavadora
            // 
            this.btLavadora.Image = global::Comercializadora.Properties.Resources.Lavadora1;
            this.btLavadora.Location = new System.Drawing.Point(226, 203);
            this.btLavadora.Name = "btLavadora";
            this.btLavadora.Size = new System.Drawing.Size(84, 116);
            this.btLavadora.TabIndex = 35;
            this.btLavadora.UseVisualStyleBackColor = true;
            this.btLavadora.Click += new System.EventHandler(this.btLavadora_Click);
            // 
            // btRefrigerador
            // 
            this.btRefrigerador.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btRefrigerador.Image = global::Comercializadora.Properties.Resources.refrigerador;
            this.btRefrigerador.Location = new System.Drawing.Point(31, 202);
            this.btRefrigerador.Name = "btRefrigerador";
            this.btRefrigerador.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btRefrigerador.Size = new System.Drawing.Size(83, 117);
            this.btRefrigerador.TabIndex = 34;
            this.btRefrigerador.UseVisualStyleBackColor = true;
            this.btRefrigerador.Click += new System.EventHandler(this.btRefrigerador_Click);
            // 
            // LineaBlanca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 501);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BtAireMabe);
            this.Controls.Add(this.BtAireCarrier);
            this.Controls.Add(this.BtAireLg);
            this.Controls.Add(this.BtAireSamsung);
            this.Controls.Add(this.BtMicroDaewoo);
            this.Controls.Add(this.BtMicroMabe);
            this.Controls.Add(this.BtMicroWhirlpool);
            this.Controls.Add(this.BtMicroSamsung);
            this.Controls.Add(this.BtLavaLg);
            this.Controls.Add(this.BtLavaGeneral);
            this.Controls.Add(this.BtLavaWhirlpool);
            this.Controls.Add(this.BtLavaSamsung);
            this.Controls.Add(this.BtRefriMabe);
            this.Controls.Add(this.BtRefriLg);
            this.Controls.Add(this.BtRefriWhirlpool);
            this.Controls.Add(this.BtRefriSamsung);
            this.Controls.Add(this.BtVerTodoMicro);
            this.Controls.Add(this.BtVerTodoLava);
            this.Controls.Add(this.BtVerTodoRefri);
            this.Controls.Add(this.BtVerTodoAire);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btAire);
            this.Controls.Add(this.btMicro);
            this.Controls.Add(this.btLavadora);
            this.Controls.Add(this.btRefrigerador);
            this.Name = "LineaBlanca";
            this.Text = "LineaBlanca";
            this.Load += new System.EventHandler(this.LineaBlanca_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button BtVerTodoAire;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btAire;
        private System.Windows.Forms.Button btMicro;
        private System.Windows.Forms.Button btLavadora;
        private System.Windows.Forms.Button btRefrigerador;
        private System.Windows.Forms.Button BtVerTodoRefri;
        private System.Windows.Forms.Button BtVerTodoLava;
        private System.Windows.Forms.Button BtVerTodoMicro;
        private System.Windows.Forms.Button BtRefriSamsung;
        private System.Windows.Forms.Button BtRefriWhirlpool;
        private System.Windows.Forms.Button BtRefriLg;
        private System.Windows.Forms.Button BtRefriMabe;
        private System.Windows.Forms.Button BtLavaSamsung;
        private System.Windows.Forms.Button BtLavaWhirlpool;
        private System.Windows.Forms.Button BtLavaGeneral;
        private System.Windows.Forms.Button BtLavaLg;
        private System.Windows.Forms.Button BtMicroSamsung;
        private System.Windows.Forms.Button BtMicroWhirlpool;
        private System.Windows.Forms.Button BtMicroMabe;
        private System.Windows.Forms.Button BtMicroDaewoo;
        private System.Windows.Forms.Button BtAireSamsung;
        private System.Windows.Forms.Button BtAireLg;
        private System.Windows.Forms.Button BtAireCarrier;
        private System.Windows.Forms.Button BtAireMabe;
        private System.Windows.Forms.Button button1;
    }
}