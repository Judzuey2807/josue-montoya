﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class LineaBlanca : Form
    {
        public LineaBlanca()
        {
            InitializeComponent();
        }

        private void LineaBlanca_Load(object sender, EventArgs e)
        {

        }

        private void btRefrigerador_Click(object sender, EventArgs e)
        {

        }

        private void btLavadora_Click(object sender, EventArgs e)
        {

        }

        private void btMicro_Click(object sender, EventArgs e)
        {

        }

        private void btAire_Click(object sender, EventArgs e)
        {

        }

        private void clbRefrigerador_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void clbLavadora_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void clbMicro_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void clbAire_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bReSeleccionar_Click(object sender, EventArgs e)
        {



        }

        private void bLavaSeleccionar_Click(object sender, EventArgs e)
        {


        }

        private void bMicroSeleccionar_Click(object sender, EventArgs e)
        {


        }

        private void bAireSeleccionar_Click(object sender, EventArgs e)
        {

        }

        private void bVerTodo_Click(object sender, EventArgs e)
        {
            VerMarcasAires btVer = new VerMarcasAires();
            VerMarcasAires ver = btVer;
            ver.Show();
            this.Dispose();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void BtAceptarRefri_Click(object sender, EventArgs e)
        {
            VerMarcasRefri btMarcas = new VerMarcasRefri();
            VerMarcasRefri ver = btMarcas;
            ver.Show();
            this.Dispose();

        }

        private void btAceptarLava_Click(object sender, EventArgs e)
        {
            VerMarcasLavadoras Btver = new VerMarcasLavadoras();
            VerMarcasLavadoras ver = Btver;
            ver.Show();
            this.Dispose();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AireSamsung btairesamsung = new AireSamsung();
            AireSamsung ver = btairesamsung;
            ver.Show();
            this.Dispose();
        }

        private void BtAireLg_Click(object sender, EventArgs e)
        {
            AireLg Btlg = new AireLg();
            AireLg ver = Btlg;
            ver.Show();
            this.Dispose();
        }

        private void BtAireMabe_Click(object sender, EventArgs e)
        {
            AireMabe btMabe = new AireMabe();
            AireMabe ver = btMabe;
            ver.Show();
            this.Dispose();
        }

        private void BtAireCarrier_Click(object sender, EventArgs e)
        {
            AireCarrier btCarrier = new AireCarrier();
            AireCarrier ver = btCarrier;
            ver.Show();
            this.Dispose();
        }

        private void BtMicroSamsung_Click(object sender, EventArgs e)
        {
            MicroSamsung btMicro = new MicroSamsung();
            MicroSamsung ver = btMicro;
            ver.Show();
            this.Dispose();
        }

        private void BtMicroWhirlpool_Click(object sender, EventArgs e)
        {
            MicroWhirlpool btWhirlpool = new MicroWhirlpool();
            MicroWhirlpool ver = btWhirlpool;
            ver.Show();
            this.Dispose();
        }

        private void BtMicroMabe_Click(object sender, EventArgs e)
        {
            MicroMabe btMabe = new MicroMabe();
            MicroMabe ver = btMabe;
            ver.Show();
            this.Dispose();
        }

        private void BtMicroDaewoo_Click(object sender, EventArgs e)
        {
            MicroDaewoo btDaewoo = new MicroDaewoo();
            MicroDaewoo ver = btDaewoo;
            ver.Show();
            this.Dispose();
        }

        private void BtLavaSamsung_Click(object sender, EventArgs e)
        {
            LavaSamsung btSamsung = new LavaSamsung();
            LavaSamsung ver = btSamsung;
            ver.Show();
            this.Dispose();
        }

        private void BtLavaWhirlpool_Click(object sender, EventArgs e)
        {
            LavaWhirlpool btWhirlpool = new LavaWhirlpool();
            LavaWhirlpool ver = btWhirlpool;
            ver.Show();
            this.Dispose();
        }

        private void BtLavaLg_Click(object sender, EventArgs e)
        {
            LavaLg btLg = new LavaLg();
            LavaLg ver = btLg;
            ver.Show();
            this.Dispose();
        }

        private void BtLavaGeneral_Click(object sender, EventArgs e)
        {
            LavaGenericElectric btGeneric = new LavaGenericElectric();
            LavaGenericElectric ver = btGeneric;
            ver.Show();
            this.Dispose();
        }

        private void BtRefriSamsung_Click(object sender, EventArgs e)
        {
            RefriSamsung btSamsung = new RefriSamsung();
            RefriSamsung ver = btSamsung;
            ver.Show();
            this.Dispose();
        }

        private void BtRefriMabe_Click(object sender, EventArgs e)
        {
            RefriMabe btMabe = new RefriMabe();
            RefriMabe ver = btMabe;
            ver.Show();
            this.Dispose();
        }

        private void BtRefriLg_Click(object sender, EventArgs e)
        {
            RefriLg btLg = new RefriLg();
            RefriLg ver = btLg;
            ver.Show();
            this.Dispose();
        }

        private void BtRefriWhirlpool_Click(object sender, EventArgs e)
        {
            RefriWhirlpool btwhirlpool = new RefriWhirlpool();
            RefriWhirlpool ver = btwhirlpool;
            ver.Show();
            this.Dispose();
        }

        private void BtVerTodoMicro_Click(object sender, EventArgs e)
        {
            VerTodoMicroondas btVer = new VerTodoMicroondas();
            VerTodoMicroondas ver = btVer;
            ver.Show();
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VerTodosLosProductos btProductos = new VerTodosLosProductos();
            VerTodosLosProductos ver = btProductos;
            ver.Show();
            this.Dispose();
        }
    }
}

