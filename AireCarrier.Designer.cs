﻿namespace Comercializadora
{
    partial class AireCarrier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AireCarrier));
            this.ComprarAireCarrier3 = new System.Windows.Forms.Button();
            this.AgregarAireCarrier3 = new System.Windows.Forms.Button();
            this.AgregarAireCarrier2 = new System.Windows.Forms.Button();
            this.ComprarAireCarrier2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarAireCarrier1 = new System.Windows.Forms.Button();
            this.ComprarAireCarrier1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtRegresarCarrier = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ComprarAireCarrier3
            // 
            this.ComprarAireCarrier3.Location = new System.Drawing.Point(254, 715);
            this.ComprarAireCarrier3.Name = "ComprarAireCarrier3";
            this.ComprarAireCarrier3.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireCarrier3.TabIndex = 131;
            this.ComprarAireCarrier3.Text = "Comprar";
            this.ComprarAireCarrier3.UseVisualStyleBackColor = true;
            // 
            // AgregarAireCarrier3
            // 
            this.AgregarAireCarrier3.Location = new System.Drawing.Point(382, 715);
            this.AgregarAireCarrier3.Name = "AgregarAireCarrier3";
            this.AgregarAireCarrier3.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireCarrier3.TabIndex = 130;
            this.AgregarAireCarrier3.Text = "Agregar al carrito";
            this.AgregarAireCarrier3.UseVisualStyleBackColor = true;
            // 
            // AgregarAireCarrier2
            // 
            this.AgregarAireCarrier2.Location = new System.Drawing.Point(382, 532);
            this.AgregarAireCarrier2.Name = "AgregarAireCarrier2";
            this.AgregarAireCarrier2.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireCarrier2.TabIndex = 129;
            this.AgregarAireCarrier2.Text = "Agregar al carrito";
            this.AgregarAireCarrier2.UseVisualStyleBackColor = true;
            // 
            // ComprarAireCarrier2
            // 
            this.ComprarAireCarrier2.Location = new System.Drawing.Point(254, 532);
            this.ComprarAireCarrier2.Name = "ComprarAireCarrier2";
            this.ComprarAireCarrier2.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireCarrier2.TabIndex = 128;
            this.ComprarAireCarrier2.Text = "Comprar";
            this.ComprarAireCarrier2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(251, 660);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(306, 39);
            this.label12.TabIndex = 127;
            this.label12.Text = "Recubrimiento Blue Fin, protege las tuberías contra la corrosión\r\nFiltro Silver I" +
    "on que elimina bacterias\r\nCon Gas Ecológico R410A para ahorrar energía";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(251, 633);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 126;
            this.label11.Text = "$ 6,590.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(251, 608);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 15);
            this.label10.TabIndex = 125;
            this.label10.Text = "Marca: CARRIER";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(250, 586);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(318, 22);
            this.label9.TabIndex = 124;
            this.label9.Text = "Mini Split  Frío y Calor 12000 BTU";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(251, 452);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(384, 65);
            this.label8.TabIndex = 123;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(246, 426);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 15);
            this.label7.TabIndex = 122;
            this.label7.Text = "$ 4,990.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(245, 401);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 15);
            this.label6.TabIndex = 121;
            this.label6.Text = "Marca:  CARRIER";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(244, 379);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(256, 22);
            this.label5.TabIndex = 120;
            this.label5.Text = "Aire Acondicionado 12000 ";
            // 
            // AgregarAireCarrier1
            // 
            this.AgregarAireCarrier1.Location = new System.Drawing.Point(382, 332);
            this.AgregarAireCarrier1.Name = "AgregarAireCarrier1";
            this.AgregarAireCarrier1.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireCarrier1.TabIndex = 119;
            this.AgregarAireCarrier1.Text = "Agregar al carrito";
            this.AgregarAireCarrier1.UseVisualStyleBackColor = true;
            // 
            // ComprarAireCarrier1
            // 
            this.ComprarAireCarrier1.Location = new System.Drawing.Point(248, 332);
            this.ComprarAireCarrier1.Name = "ComprarAireCarrier1";
            this.ComprarAireCarrier1.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireCarrier1.TabIndex = 118;
            this.ComprarAireCarrier1.Text = "Comprar";
            this.ComprarAireCarrier1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(251, 281);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 39);
            this.label4.TabIndex = 117;
            this.label4.Text = "1 tonelada\r\n18 SEER\r\nR410A";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(252, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 116;
            this.label3.Text = "$ 6,600.71";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(250, 227);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 15);
            this.label2.TabIndex = 115;
            this.label2.Text = "Marca: CARRIER";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(250, 183);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(445, 44);
            this.label1.TabIndex = 114;
            this.label1.Text = "CARRIER AIRE ACONDICIONADO MINISPLIT LIV\r\n12000 BTU\'S BLANCO";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.AireCarrier2;
            this.pictureBox4.Location = new System.Drawing.Point(12, 411);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(214, 94);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.AireCarrier3;
            this.pictureBox3.Location = new System.Drawing.Point(12, 605);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(214, 94);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.AireCarrier;
            this.pictureBox2.Location = new System.Drawing.Point(12, 205);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(214, 94);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.Carrier;
            this.pictureBox1.Location = new System.Drawing.Point(186, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(448, 142);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BtRegresarCarrier
            // 
            this.BtRegresarCarrier.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarCarrier.Location = new System.Drawing.Point(0, -1);
            this.BtRegresarCarrier.Name = "BtRegresarCarrier";
            this.BtRegresarCarrier.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarCarrier.TabIndex = 139;
            this.BtRegresarCarrier.UseVisualStyleBackColor = true;
            this.BtRegresarCarrier.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // AireCarrier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(812, 758);
            this.Controls.Add(this.BtRegresarCarrier);
            this.Controls.Add(this.ComprarAireCarrier3);
            this.Controls.Add(this.AgregarAireCarrier3);
            this.Controls.Add(this.AgregarAireCarrier2);
            this.Controls.Add(this.ComprarAireCarrier2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarAireCarrier1);
            this.Controls.Add(this.ComprarAireCarrier1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "AireCarrier";
            this.Text = "-";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button ComprarAireCarrier3;
        private System.Windows.Forms.Button AgregarAireCarrier3;
        private System.Windows.Forms.Button AgregarAireCarrier2;
        private System.Windows.Forms.Button ComprarAireCarrier2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarAireCarrier1;
        private System.Windows.Forms.Button ComprarAireCarrier1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtRegresarCarrier;
    }
}