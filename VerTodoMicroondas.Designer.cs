﻿namespace Comercializadora
{
    partial class VerTodoMicroondas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VerTodoMicroondas));
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            this.ComprarVerTodoMicroSamsung3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoMicroSamsung3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoMicroSamsung2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoMicroSamsung2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarVerTodoMicroSamsung1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoMicroSamsung1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ComprarVerTodoMicroWhirlpool3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoMicroWhirlpool3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoMicroWhirlpool2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoMicroWhirlpool2 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.AgregarVerTodoMicroWhirlpool1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoMicroWhirlpool1 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.ComprarVerTodoMicroMabe3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoMicroMabe3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoMicroMabe2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoMicroMabe2 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.AgregarVerTodoMicroMabe1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoMicroMabe1 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.ComprarVerTodoMicroDaewoo3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoMicroDaewoo3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoMicroDaewoo2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoMicroDaewoo2 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.AgregarVerTodoMicroDaewoo1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoMicroDaewoo1 = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.SuspendLayout();
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(0, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 143;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // ComprarVerTodoMicroSamsung3
            // 
            this.ComprarVerTodoMicroSamsung3.Location = new System.Drawing.Point(281, 719);
            this.ComprarVerTodoMicroSamsung3.Name = "ComprarVerTodoMicroSamsung3";
            this.ComprarVerTodoMicroSamsung3.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroSamsung3.TabIndex = 165;
            this.ComprarVerTodoMicroSamsung3.Text = "Comprar";
            this.ComprarVerTodoMicroSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoMicroSamsung3
            // 
            this.AgregarVerTodoMicroSamsung3.Location = new System.Drawing.Point(419, 719);
            this.AgregarVerTodoMicroSamsung3.Name = "AgregarVerTodoMicroSamsung3";
            this.AgregarVerTodoMicroSamsung3.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroSamsung3.TabIndex = 164;
            this.AgregarVerTodoMicroSamsung3.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoMicroSamsung2
            // 
            this.AgregarVerTodoMicroSamsung2.Location = new System.Drawing.Point(419, 497);
            this.AgregarVerTodoMicroSamsung2.Name = "AgregarVerTodoMicroSamsung2";
            this.AgregarVerTodoMicroSamsung2.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroSamsung2.TabIndex = 163;
            this.AgregarVerTodoMicroSamsung2.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroSamsung2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoMicroSamsung2
            // 
            this.ComprarVerTodoMicroSamsung2.Location = new System.Drawing.Point(290, 497);
            this.ComprarVerTodoMicroSamsung2.Name = "ComprarVerTodoMicroSamsung2";
            this.ComprarVerTodoMicroSamsung2.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroSamsung2.TabIndex = 162;
            this.ComprarVerTodoMicroSamsung2.Text = "Comprar";
            this.ComprarVerTodoMicroSamsung2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(277, 662);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(246, 39);
            this.label12.TabIndex = 161;
            this.label12.Text = "5 niveles de potencia\r\n3 Etapas de cocción en un solo toque\r\n10 Botones programab" +
    "les y 20 memorias diferentes";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(285, 637);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 160;
            this.label11.Text = "$ 5,390.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(278, 611);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 15);
            this.label10.TabIndex = 159;
            this.label10.Text = "Marca: SAMSUNG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(276, 558);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(547, 44);
            this.label9.TabIndex = 158;
            this.label9.Text = "Horno de Microondas Daewoo Industrial 1.0 Pies Cúbicos \r\nAcero Inox";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(288, 413);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 65);
            this.label8.TabIndex = 157;
            this.label8.Text = "- Fácil Limpieza\r\n- Tamaño: 33.7 x 65.3 x 50.4\r\n- Con plato giratorio\r\n- Botón Pa" +
    "lomitas\r\n- 6 opciones de cocción";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(288, 389);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 15);
            this.label7.TabIndex = 156;
            this.label7.Text = "$ 2190.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(288, 362);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 15);
            this.label6.TabIndex = 155;
            this.label6.Text = "Marca: SAMSUNG";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(293, 318);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(324, 44);
            this.label5.TabIndex = 154;
            this.label5.Text = "Horno de Microondas\r\n1.6 Pies Cúbicos Acero Inoxidable";
            // 
            // AgregarVerTodoMicroSamsung1
            // 
            this.AgregarVerTodoMicroSamsung1.Location = new System.Drawing.Point(419, 253);
            this.AgregarVerTodoMicroSamsung1.Name = "AgregarVerTodoMicroSamsung1";
            this.AgregarVerTodoMicroSamsung1.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroSamsung1.TabIndex = 153;
            this.AgregarVerTodoMicroSamsung1.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroSamsung1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoMicroSamsung1
            // 
            this.ComprarVerTodoMicroSamsung1.Location = new System.Drawing.Point(280, 253);
            this.ComprarVerTodoMicroSamsung1.Name = "ComprarVerTodoMicroSamsung1";
            this.ComprarVerTodoMicroSamsung1.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroSamsung1.TabIndex = 152;
            this.ComprarVerTodoMicroSamsung1.Text = "Comprar";
            this.ComprarVerTodoMicroSamsung1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(288, 195);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 39);
            this.label4.TabIndex = 151;
            this.label4.Text = "Fácil Limpieza\r\nTamaño: 30.1 x 55.8 x 43.6\r\nCon plato giratorio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(288, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 150;
            this.label3.Text = "$ 2,590.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(288, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 15);
            this.label2.TabIndex = 149;
            this.label2.Text = "Marca: SAMSUNG";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(287, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(514, 22);
            this.label1.TabIndex = 148;
            this.label1.Text = "Horno de Microondas Samsung 1.4 Pies Cúbicos Plata\r\n";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.MicroSamsung2;
            this.pictureBox4.Location = new System.Drawing.Point(15, 340);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(227, 129);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 147;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.MicroSamsung3;
            this.pictureBox3.Location = new System.Drawing.Point(15, 577);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(227, 135);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 146;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.MicroSamsung1;
            this.pictureBox2.Location = new System.Drawing.Point(12, 124);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(230, 129);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 145;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.samsung1;
            this.pictureBox1.Location = new System.Drawing.Point(297, 46);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 144;
            this.pictureBox1.TabStop = false;
            // 
            // ComprarVerTodoMicroWhirlpool3
            // 
            this.ComprarVerTodoMicroWhirlpool3.Location = new System.Drawing.Point(283, 1431);
            this.ComprarVerTodoMicroWhirlpool3.Name = "ComprarVerTodoMicroWhirlpool3";
            this.ComprarVerTodoMicroWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroWhirlpool3.TabIndex = 187;
            this.ComprarVerTodoMicroWhirlpool3.Text = "Comprar";
            this.ComprarVerTodoMicroWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoMicroWhirlpool3
            // 
            this.AgregarVerTodoMicroWhirlpool3.Location = new System.Drawing.Point(422, 1431);
            this.AgregarVerTodoMicroWhirlpool3.Name = "AgregarVerTodoMicroWhirlpool3";
            this.AgregarVerTodoMicroWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroWhirlpool3.TabIndex = 186;
            this.AgregarVerTodoMicroWhirlpool3.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoMicroWhirlpool2
            // 
            this.AgregarVerTodoMicroWhirlpool2.Location = new System.Drawing.Point(422, 1240);
            this.AgregarVerTodoMicroWhirlpool2.Name = "AgregarVerTodoMicroWhirlpool2";
            this.AgregarVerTodoMicroWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroWhirlpool2.TabIndex = 185;
            this.AgregarVerTodoMicroWhirlpool2.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoMicroWhirlpool2
            // 
            this.ComprarVerTodoMicroWhirlpool2.Location = new System.Drawing.Point(283, 1240);
            this.ComprarVerTodoMicroWhirlpool2.Name = "ComprarVerTodoMicroWhirlpool2";
            this.ComprarVerTodoMicroWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroWhirlpool2.TabIndex = 184;
            this.ComprarVerTodoMicroWhirlpool2.Text = "Comprar";
            this.ComprarVerTodoMicroWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(291, 1368);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(181, 39);
            this.label13.TabIndex = 183;
            this.label13.Text = "10 Niveles de potencia\r\nFunciones predeterminadas\r\nDescongelamiento por peso y ti" +
    "empo";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(281, 1343);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 15);
            this.label14.TabIndex = 182;
            this.label14.Text = "$ 2,290.00";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(280, 1317);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 15);
            this.label15.TabIndex = 181;
            this.label15.Text = "Marca: WHIRLPOOL";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(280, 1295);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(463, 22);
            this.label16.TabIndex = 180;
            this.label16.Text = "Horno de Microondas Whirlpool 1.1 Pies Cúbicos";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(291, 1180);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(205, 39);
            this.label17.TabIndex = 179;
            this.label17.Text = "- Funciones predeterminadas de alimentos\r\n- 3 opciones de descongelar por peso\r\n-" +
    " Diseño moderno";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(291, 1153);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 15);
            this.label18.TabIndex = 178;
            this.label18.Text = "$ 2190.00";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(291, 1118);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 15);
            this.label19.TabIndex = 177;
            this.label19.Text = "Marca: WHIRLPOOL";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label20.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(296, 1096);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(470, 22);
            this.label20.TabIndex = 176;
            this.label20.Text = "Horno de Microondas Whirlpool WM1511D Blanco";
            // 
            // AgregarVerTodoMicroWhirlpool1
            // 
            this.AgregarVerTodoMicroWhirlpool1.Location = new System.Drawing.Point(422, 1031);
            this.AgregarVerTodoMicroWhirlpool1.Name = "AgregarVerTodoMicroWhirlpool1";
            this.AgregarVerTodoMicroWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroWhirlpool1.TabIndex = 175;
            this.AgregarVerTodoMicroWhirlpool1.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoMicroWhirlpool1
            // 
            this.ComprarVerTodoMicroWhirlpool1.Location = new System.Drawing.Point(283, 1031);
            this.ComprarVerTodoMicroWhirlpool1.Name = "ComprarVerTodoMicroWhirlpool1";
            this.ComprarVerTodoMicroWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroWhirlpool1.TabIndex = 174;
            this.ComprarVerTodoMicroWhirlpool1.Text = "Comprar";
            this.ComprarVerTodoMicroWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(291, 973);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(155, 39);
            this.label21.TabIndex = 173;
            this.label21.Text = "9 opciones para cocinar\r\nDescongela alimentos por peso\r\nBloqueo de controles";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(291, 948);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 15);
            this.label22.TabIndex = 172;
            this.label22.Text = "$ 2,035.00";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(291, 924);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(120, 15);
            this.label23.TabIndex = 171;
            this.label23.Text = "Marca: WHIRLPOOL";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(290, 902);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(521, 22);
            this.label24.TabIndex = 170;
            this.label24.Text = "Horno de Microondas Whirlpool 1.1 Pies Cúbicos Silver";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Comercializadora.Properties.Resources.MicroWhirlpool3;
            this.pictureBox5.Location = new System.Drawing.Point(35, 1295);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(216, 131);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox5.TabIndex = 169;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Comercializadora.Properties.Resources.MicroWhirlpool2;
            this.pictureBox6.Location = new System.Drawing.Point(35, 1096);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(216, 123);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox6.TabIndex = 168;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Comercializadora.Properties.Resources.MicroWhirlpool1;
            this.pictureBox7.Location = new System.Drawing.Point(35, 902);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(216, 132);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox7.TabIndex = 167;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Comercializadora.Properties.Resources.whirlpool;
            this.pictureBox8.Location = new System.Drawing.Point(291, 778);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(237, 104);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox8.TabIndex = 166;
            this.pictureBox8.TabStop = false;
            // 
            // ComprarVerTodoMicroMabe3
            // 
            this.ComprarVerTodoMicroMabe3.Location = new System.Drawing.Point(276, 2150);
            this.ComprarVerTodoMicroMabe3.Name = "ComprarVerTodoMicroMabe3";
            this.ComprarVerTodoMicroMabe3.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroMabe3.TabIndex = 209;
            this.ComprarVerTodoMicroMabe3.Text = "Comprar";
            this.ComprarVerTodoMicroMabe3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoMicroMabe3
            // 
            this.AgregarVerTodoMicroMabe3.Location = new System.Drawing.Point(414, 2150);
            this.AgregarVerTodoMicroMabe3.Name = "AgregarVerTodoMicroMabe3";
            this.AgregarVerTodoMicroMabe3.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroMabe3.TabIndex = 208;
            this.AgregarVerTodoMicroMabe3.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroMabe3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoMicroMabe2
            // 
            this.AgregarVerTodoMicroMabe2.Location = new System.Drawing.Point(414, 1919);
            this.AgregarVerTodoMicroMabe2.Name = "AgregarVerTodoMicroMabe2";
            this.AgregarVerTodoMicroMabe2.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroMabe2.TabIndex = 207;
            this.AgregarVerTodoMicroMabe2.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroMabe2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoMicroMabe2
            // 
            this.ComprarVerTodoMicroMabe2.Location = new System.Drawing.Point(276, 1919);
            this.ComprarVerTodoMicroMabe2.Name = "ComprarVerTodoMicroMabe2";
            this.ComprarVerTodoMicroMabe2.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroMabe2.TabIndex = 206;
            this.ComprarVerTodoMicroMabe2.Text = "Comprar";
            this.ComprarVerTodoMicroMabe2.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(283, 2051);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(386, 91);
            this.label25.TabIndex = 205;
            this.label25.Text = resources.GetString("label25.Text");
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(273, 2026);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(58, 15);
            this.label26.TabIndex = 204;
            this.label26.Text = " 1,743.00";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(272, 2000);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 15);
            this.label27.TabIndex = 203;
            this.label27.Text = "Marca: MABE";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(272, 1978);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(476, 22);
            this.label28.TabIndex = 202;
            this.label28.Text = "Horno Microondas Mabe 0.7 Pies Mabe HMM70SW";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(283, 1861);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(171, 39);
            this.label29.TabIndex = 201;
            this.label29.Text = "10 niveles de potencia\r\n6 selecciones rápidas de cocinado\r\nSeguro para niños";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(283, 1846);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(65, 15);
            this.label30.TabIndex = 200;
            this.label30.Text = "$ 2,900.00";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(283, 1821);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 15);
            this.label31.TabIndex = 199;
            this.label31.Text = "Marca: MABE";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label32.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(282, 1777);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(309, 44);
            this.label32.TabIndex = 198;
            this.label32.Text = "HORNO DE MICROONDAS 1.4 P3\r\nCUFT ESPEJO PLATA Mabe ";
            // 
            // AgregarVerTodoMicroMabe1
            // 
            this.AgregarVerTodoMicroMabe1.Location = new System.Drawing.Point(414, 1714);
            this.AgregarVerTodoMicroMabe1.Name = "AgregarVerTodoMicroMabe1";
            this.AgregarVerTodoMicroMabe1.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroMabe1.TabIndex = 197;
            this.AgregarVerTodoMicroMabe1.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroMabe1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoMicroMabe1
            // 
            this.ComprarVerTodoMicroMabe1.Location = new System.Drawing.Point(275, 1714);
            this.ComprarVerTodoMicroMabe1.Name = "ComprarVerTodoMicroMabe1";
            this.ComprarVerTodoMicroMabe1.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroMabe1.TabIndex = 196;
            this.ComprarVerTodoMicroMabe1.Text = "Comprar";
            this.ComprarVerTodoMicroMabe1.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(283, 1656);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(128, 39);
            this.label33.TabIndex = 195;
            this.label33.Text = "Fácil Limpieza\r\nTamaño: 26.2 x 45.2 x 33\r\nCon plato giratorio";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(283, 1631);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(55, 15);
            this.label34.TabIndex = 194;
            this.label34.Text = "$ 990.00";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(283, 1607);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(82, 15);
            this.label35.TabIndex = 193;
            this.label35.Text = "Marca: MABE";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(282, 1585);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(496, 22);
            this.label36.TabIndex = 192;
            this.label36.Text = "Horno de Microondas Mabe 0.7 Pies Cúbicos Blanco";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Comercializadora.Properties.Resources.MicroMabe3;
            this.pictureBox9.Location = new System.Drawing.Point(28, 1978);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(229, 139);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox9.TabIndex = 191;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Comercializadora.Properties.Resources.MicroMabe2;
            this.pictureBox10.Location = new System.Drawing.Point(28, 1792);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(220, 129);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox10.TabIndex = 190;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Comercializadora.Properties.Resources.MicroMabe1;
            this.pictureBox11.Location = new System.Drawing.Point(28, 1596);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(220, 130);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox11.TabIndex = 189;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Comercializadora.Properties.Resources.Mabe;
            this.pictureBox12.Location = new System.Drawing.Point(302, 1477);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(226, 87);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox12.TabIndex = 188;
            this.pictureBox12.TabStop = false;
            // 
            // ComprarVerTodoMicroDaewoo3
            // 
            this.ComprarVerTodoMicroDaewoo3.Location = new System.Drawing.Point(278, 2878);
            this.ComprarVerTodoMicroDaewoo3.Name = "ComprarVerTodoMicroDaewoo3";
            this.ComprarVerTodoMicroDaewoo3.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroDaewoo3.TabIndex = 231;
            this.ComprarVerTodoMicroDaewoo3.Text = "Comprar";
            this.ComprarVerTodoMicroDaewoo3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoMicroDaewoo3
            // 
            this.AgregarVerTodoMicroDaewoo3.Location = new System.Drawing.Point(417, 2878);
            this.AgregarVerTodoMicroDaewoo3.Name = "AgregarVerTodoMicroDaewoo3";
            this.AgregarVerTodoMicroDaewoo3.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroDaewoo3.TabIndex = 230;
            this.AgregarVerTodoMicroDaewoo3.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroDaewoo3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoMicroDaewoo2
            // 
            this.AgregarVerTodoMicroDaewoo2.Location = new System.Drawing.Point(417, 2704);
            this.AgregarVerTodoMicroDaewoo2.Name = "AgregarVerTodoMicroDaewoo2";
            this.AgregarVerTodoMicroDaewoo2.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroDaewoo2.TabIndex = 229;
            this.AgregarVerTodoMicroDaewoo2.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroDaewoo2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoMicroDaewoo2
            // 
            this.ComprarVerTodoMicroDaewoo2.Location = new System.Drawing.Point(279, 2704);
            this.ComprarVerTodoMicroDaewoo2.Name = "ComprarVerTodoMicroDaewoo2";
            this.ComprarVerTodoMicroDaewoo2.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoMicroDaewoo2.TabIndex = 228;
            this.ComprarVerTodoMicroDaewoo2.Text = "Comprar";
            this.ComprarVerTodoMicroDaewoo2.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(276, 2824);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(269, 39);
            this.label37.TabIndex = 227;
            this.label37.Text = "- Función de descongelamiento por tiempo o por peso\r\n- 4 opciones de cocción rápi" +
    "do\r\n- Bloqueo de panel para evitar accidentes con los niños";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(276, 2793);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 15);
            this.label38.TabIndex = 226;
            this.label38.Text = "$ 2,290.00";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label39.Location = new System.Drawing.Point(276, 2764);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(102, 15);
            this.label39.TabIndex = 225;
            this.label39.Text = "Marca: DAEWOO";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label40.Location = new System.Drawing.Point(275, 2742);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(509, 22);
            this.label40.TabIndex = 224;
            this.label40.Text = "Horno de Microondas Daewoo 1.1 Pies Cúbicos Acero";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(270, 2592);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(211, 91);
            this.label41.TabIndex = 223;
            this.label41.Text = resources.GetString("label41.Text");
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label42.Location = new System.Drawing.Point(270, 2565);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(65, 15);
            this.label42.TabIndex = 222;
            this.label42.Text = "$ 1,199.00";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(270, 2539);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(102, 15);
            this.label43.TabIndex = 221;
            this.label43.Text = "Marca: DAEWOO";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label44.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(269, 2517);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(469, 22);
            this.label44.TabIndex = 220;
            this.label44.Text = "Horno De Microondas 0.7 Pies Daewoo KOR-660S";
            // 
            // AgregarVerTodoMicroDaewoo1
            // 
            this.AgregarVerTodoMicroDaewoo1.Location = new System.Drawing.Point(417, 2482);
            this.AgregarVerTodoMicroDaewoo1.Name = "AgregarVerTodoMicroDaewoo1";
            this.AgregarVerTodoMicroDaewoo1.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoMicroDaewoo1.TabIndex = 219;
            this.AgregarVerTodoMicroDaewoo1.Text = "Agregar al carrito";
            this.AgregarVerTodoMicroDaewoo1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoMicroDaewoo1
            // 
            this.ComprarVerTodoMicroDaewoo1.Location = new System.Drawing.Point(273, 2482);
            this.ComprarVerTodoMicroDaewoo1.Name = "ComprarVerTodoMicroDaewoo1";
            this.ComprarVerTodoMicroDaewoo1.Size = new System.Drawing.Size(110, 23);
            this.ComprarVerTodoMicroDaewoo1.TabIndex = 218;
            this.ComprarVerTodoMicroDaewoo1.Text = "Comprar";
            this.ComprarVerTodoMicroDaewoo1.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(270, 2385);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(199, 78);
            this.label45.TabIndex = 217;
            this.label45.Text = "Sistema de cóncavo de reflexión\r\nOperación fácil\r\nOpciones de Descongelado por al" +
    "imento\r\n5 Menús de auto cocción\r\n10 Niveles de potencia\r\nBotón de inicio rápido";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label46.Location = new System.Drawing.Point(270, 2360);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(0, 15);
            this.label46.TabIndex = 216;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(270, 2336);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(102, 15);
            this.label47.TabIndex = 215;
            this.label47.Text = "Marca: DAEWOO";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label48.Location = new System.Drawing.Point(269, 2314);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(353, 22);
            this.label48.TabIndex = 214;
            this.label48.Text = "Horno Daewoo DAEWOO KOR-1N0AS";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Comercializadora.Properties.Resources.MicroDaewoo3;
            this.pictureBox13.Location = new System.Drawing.Point(7, 2742);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(250, 145);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox13.TabIndex = 213;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Comercializadora.Properties.Resources.MicroDaewoo2;
            this.pictureBox14.Location = new System.Drawing.Point(8, 2526);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(249, 145);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox14.TabIndex = 212;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Comercializadora.Properties.Resources.MicroDaewoo1;
            this.pictureBox15.Location = new System.Drawing.Point(11, 2327);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(246, 145);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox15.TabIndex = 211;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Comercializadora.Properties.Resources.Daewoo;
            this.pictureBox16.Location = new System.Drawing.Point(316, 2213);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(236, 66);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox16.TabIndex = 210;
            this.pictureBox16.TabStop = false;
            // 
            // VerTodoMicroondas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(851, 836);
            this.Controls.Add(this.ComprarVerTodoMicroDaewoo3);
            this.Controls.Add(this.AgregarVerTodoMicroDaewoo3);
            this.Controls.Add(this.AgregarVerTodoMicroDaewoo2);
            this.Controls.Add(this.ComprarVerTodoMicroDaewoo2);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.AgregarVerTodoMicroDaewoo1);
            this.Controls.Add(this.ComprarVerTodoMicroDaewoo1);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.ComprarVerTodoMicroMabe3);
            this.Controls.Add(this.AgregarVerTodoMicroMabe3);
            this.Controls.Add(this.AgregarVerTodoMicroMabe2);
            this.Controls.Add(this.ComprarVerTodoMicroMabe2);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.AgregarVerTodoMicroMabe1);
            this.Controls.Add(this.ComprarVerTodoMicroMabe1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.ComprarVerTodoMicroWhirlpool3);
            this.Controls.Add(this.AgregarVerTodoMicroWhirlpool3);
            this.Controls.Add(this.AgregarVerTodoMicroWhirlpool2);
            this.Controls.Add(this.ComprarVerTodoMicroWhirlpool2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.AgregarVerTodoMicroWhirlpool1);
            this.Controls.Add(this.ComprarVerTodoMicroWhirlpool1);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.ComprarVerTodoMicroSamsung3);
            this.Controls.Add(this.AgregarVerTodoMicroSamsung3);
            this.Controls.Add(this.AgregarVerTodoMicroSamsung2);
            this.Controls.Add(this.ComprarVerTodoMicroSamsung2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarVerTodoMicroSamsung1);
            this.Controls.Add(this.ComprarVerTodoMicroSamsung1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BtRegresarMabe);
            this.Name = "VerTodoMicroondas";
            this.Text = "VerTodoMicroondas";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtRegresarMabe;
        private System.Windows.Forms.Button ComprarVerTodoMicroSamsung3;
        private System.Windows.Forms.Button AgregarVerTodoMicroSamsung3;
        private System.Windows.Forms.Button AgregarVerTodoMicroSamsung2;
        private System.Windows.Forms.Button ComprarVerTodoMicroSamsung2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarVerTodoMicroSamsung1;
        private System.Windows.Forms.Button ComprarVerTodoMicroSamsung1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button ComprarVerTodoMicroWhirlpool3;
        private System.Windows.Forms.Button AgregarVerTodoMicroWhirlpool3;
        private System.Windows.Forms.Button AgregarVerTodoMicroWhirlpool2;
        private System.Windows.Forms.Button ComprarVerTodoMicroWhirlpool2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button AgregarVerTodoMicroWhirlpool1;
        private System.Windows.Forms.Button ComprarVerTodoMicroWhirlpool1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Button ComprarVerTodoMicroMabe3;
        private System.Windows.Forms.Button AgregarVerTodoMicroMabe3;
        private System.Windows.Forms.Button AgregarVerTodoMicroMabe2;
        private System.Windows.Forms.Button ComprarVerTodoMicroMabe2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button AgregarVerTodoMicroMabe1;
        private System.Windows.Forms.Button ComprarVerTodoMicroMabe1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Button ComprarVerTodoMicroDaewoo3;
        private System.Windows.Forms.Button AgregarVerTodoMicroDaewoo3;
        private System.Windows.Forms.Button AgregarVerTodoMicroDaewoo2;
        private System.Windows.Forms.Button ComprarVerTodoMicroDaewoo2;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button AgregarVerTodoMicroDaewoo1;
        private System.Windows.Forms.Button ComprarVerTodoMicroDaewoo1;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
    }
}