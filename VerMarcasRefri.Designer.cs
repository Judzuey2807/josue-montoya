﻿namespace Comercializadora
{
    partial class VerMarcasRefri
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VerMarcasRefri));
            this.AgregarVerTodoRefriWhirlpool3 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriWhirlpool3 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.AgregarVerTodoRefriWhirlpool2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriWhirlpool2 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarVerTodoRefriWhirlpool1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriWhirlpool1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.AgregarVerTodoRefriSamsung3 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriSamsung3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoRefriSamsung2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriSamsung2 = new System.Windows.Forms.Button();
            this.AgregarVerTodoRefriSamsung1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriSamsung1 = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.AgregarVerTodoRefriMabe1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriMabe1 = new System.Windows.Forms.Button();
            this.AgregarVerTodoRefriMabe2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriMabe2 = new System.Windows.Forms.Button();
            this.AgregarVerTodoRefriMabe3 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriMabe3 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.AgregarVerTodoRefriLg3 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriLg3 = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.AgregarVerTodoRefriLg2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriLg2 = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.AgregarVerTodoRefriLg1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoRefriLg1 = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.SuspendLayout();
            // 
            // AgregarVerTodoRefriWhirlpool3
            // 
            this.AgregarVerTodoRefriWhirlpool3.Location = new System.Drawing.Point(310, 751);
            this.AgregarVerTodoRefriWhirlpool3.Name = "AgregarVerTodoRefriWhirlpool3";
            this.AgregarVerTodoRefriWhirlpool3.Size = new System.Drawing.Size(98, 23);
            this.AgregarVerTodoRefriWhirlpool3.TabIndex = 43;
            this.AgregarVerTodoRefriWhirlpool3.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriWhirlpool3
            // 
            this.ComprarVerTodoRefriWhirlpool3.Location = new System.Drawing.Point(192, 751);
            this.ComprarVerTodoRefriWhirlpool3.Name = "ComprarVerTodoRefriWhirlpool3";
            this.ComprarVerTodoRefriWhirlpool3.Size = new System.Drawing.Size(98, 23);
            this.ComprarVerTodoRefriWhirlpool3.TabIndex = 42;
            this.ComprarVerTodoRefriWhirlpool3.Text = "Comprar";
            this.ComprarVerTodoRefriWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(192, 675);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(432, 52);
            this.label12.TabIndex = 41;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(192, 647);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 15);
            this.label11.TabIndex = 40;
            this.label11.Text = "$ 12,746.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(192, 619);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 15);
            this.label10.TabIndex = 39;
            this.label10.Text = "Marca: WHIRLPOOL";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(192, 581);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(338, 22);
            this.label9.TabIndex = 38;
            this.label9.Text = "WHIRLPOOL REFRIGERADOR 18 P3";
            // 
            // AgregarVerTodoRefriWhirlpool2
            // 
            this.AgregarVerTodoRefriWhirlpool2.Location = new System.Drawing.Point(310, 532);
            this.AgregarVerTodoRefriWhirlpool2.Name = "AgregarVerTodoRefriWhirlpool2";
            this.AgregarVerTodoRefriWhirlpool2.Size = new System.Drawing.Size(98, 23);
            this.AgregarVerTodoRefriWhirlpool2.TabIndex = 37;
            this.AgregarVerTodoRefriWhirlpool2.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriWhirlpool2
            // 
            this.ComprarVerTodoRefriWhirlpool2.Location = new System.Drawing.Point(192, 533);
            this.ComprarVerTodoRefriWhirlpool2.Name = "ComprarVerTodoRefriWhirlpool2";
            this.ComprarVerTodoRefriWhirlpool2.Size = new System.Drawing.Size(98, 23);
            this.ComprarVerTodoRefriWhirlpool2.TabIndex = 36;
            this.ComprarVerTodoRefriWhirlpool2.Text = "Comprar";
            this.ComprarVerTodoRefriWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(189, 464);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(248, 39);
            this.label8.TabIndex = 35;
            this.label8.Text = "Recomendable para 1-2 personas\r\nMedidas fuera del paquete 168 x 56 x 67\r\nDespacha" +
    "dor de agua con capacidad de 4.2 Litros";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(192, 439);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 15);
            this.label7.TabIndex = 34;
            this.label7.Text = "$ 6,290.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(192, 411);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 15);
            this.label6.TabIndex = 33;
            this.label6.Text = "Marca: WHIRLPOOL";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(188, 354);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(456, 44);
            this.label5.TabIndex = 32;
            this.label5.Text = "Refrigerador 9 Pies Whirlpool con Despachador \r\nAcero Inox";
            // 
            // AgregarVerTodoRefriWhirlpool1
            // 
            this.AgregarVerTodoRefriWhirlpool1.Location = new System.Drawing.Point(310, 312);
            this.AgregarVerTodoRefriWhirlpool1.Name = "AgregarVerTodoRefriWhirlpool1";
            this.AgregarVerTodoRefriWhirlpool1.Size = new System.Drawing.Size(98, 23);
            this.AgregarVerTodoRefriWhirlpool1.TabIndex = 31;
            this.AgregarVerTodoRefriWhirlpool1.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriWhirlpool1
            // 
            this.ComprarVerTodoRefriWhirlpool1.Location = new System.Drawing.Point(192, 312);
            this.ComprarVerTodoRefriWhirlpool1.Name = "ComprarVerTodoRefriWhirlpool1";
            this.ComprarVerTodoRefriWhirlpool1.Size = new System.Drawing.Size(98, 23);
            this.ComprarVerTodoRefriWhirlpool1.TabIndex = 30;
            this.ComprarVerTodoRefriWhirlpool1.Text = "Comprar";
            this.ComprarVerTodoRefriWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(189, 242);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(328, 39);
            this.label4.TabIndex = 29;
            this.label4.Text = "Superficie Steel Pro Antifingerprint\r\nDispensador de agua con filtro incluido y c" +
    "apacidad de hasta 4 litros\r\nCuenta con puerta reversible y luz LED para una mayo" +
    "r visibilidad";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(192, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 28;
            this.label3.Text = "$ 7,909.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(192, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 15);
            this.label2.TabIndex = 27;
            this.label2.Text = "Marca: WHIRLPOOL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(188, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(467, 44);
            this.label1.TabIndex = 26;
            this.label1.Text = "Refrigerador 14 Pies Whirlpool con Despachador \r\nAcero inox anti huellas";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.Refriwhirlpool3;
            this.pictureBox4.Location = new System.Drawing.Point(14, 581);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(117, 194);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 25;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.RefriWhirlpool2;
            this.pictureBox3.Location = new System.Drawing.Point(14, 354);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(117, 203);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 24;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.RefriWhilpoll1;
            this.pictureBox2.Location = new System.Drawing.Point(14, 123);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(117, 212);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.whirlpool;
            this.pictureBox1.Location = new System.Drawing.Point(284, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(236, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(197, 1161);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 15);
            this.label13.TabIndex = 66;
            this.label13.Text = "Marca: SAMSUNG";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(197, 949);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(107, 15);
            this.label14.TabIndex = 65;
            this.label14.Text = "Marca: SAMSUNG";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(198, 1433);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(328, 39);
            this.label15.TabIndex = 64;
            this.label15.Text = "Recomendable para 5-6 personas\r\nMedidas fuera del paquete: 178 x 91 x 86\r\nCon 2 c" +
    "ajones en refrigerador y uno más deslizable en el congelador";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(198, 1401);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 15);
            this.label16.TabIndex = 63;
            this.label16.Text = "$ 13,790.00";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(197, 1373);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(107, 15);
            this.label17.TabIndex = 62;
            this.label17.Text = "Marca: SAMSUNG";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(197, 1347);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(350, 22);
            this.label18.TabIndex = 61;
            this.label18.Text = "Refrigerador 22 Pies Samsung Silver";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(198, 1220);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(348, 39);
            this.label19.TabIndex = 60;
            this.label19.Text = "Recomendable para 7 personas en adelante\r\nMedidas fuera del paquete 177,6 x 90,8 " +
    "x 91,9\r\nInversor digital ahorro de energía de hasta 40% vs modelo convencional";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(205, 1192);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 15);
            this.label20.TabIndex = 59;
            this.label20.Text = "$ 16,990.00";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(197, 1106);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(427, 44);
            this.label21.TabIndex = 58;
            this.label21.Text = "Refrigerador 25 Pies Samsung Despachador \r\nde Agua Acero Inox";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(198, 1008);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(255, 39);
            this.label22.TabIndex = 57;
            this.label22.Text = "Sistema de refrigeración Twing Cooling Plus\r\nInversor digital compresor\r\n70% más " +
    "humedad que un refrigerador convencional";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(197, 1008);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 13);
            this.label23.TabIndex = 56;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(205, 974);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 15);
            this.label24.TabIndex = 55;
            this.label24.Text = "$ 7,990.00";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(197, 894);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(462, 44);
            this.label25.TabIndex = 54;
            this.label25.Text = "Refrigerador 14 Pies Samsung con Despachador\r\nAcero Inox";
            // 
            // AgregarVerTodoRefriSamsung3
            // 
            this.AgregarVerTodoRefriSamsung3.Location = new System.Drawing.Point(310, 1505);
            this.AgregarVerTodoRefriSamsung3.Name = "AgregarVerTodoRefriSamsung3";
            this.AgregarVerTodoRefriSamsung3.Size = new System.Drawing.Size(95, 23);
            this.AgregarVerTodoRefriSamsung3.TabIndex = 53;
            this.AgregarVerTodoRefriSamsung3.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriSamsung3.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriSamsung3
            // 
            this.ComprarVerTodoRefriSamsung3.Location = new System.Drawing.Point(200, 1505);
            this.ComprarVerTodoRefriSamsung3.Name = "ComprarVerTodoRefriSamsung3";
            this.ComprarVerTodoRefriSamsung3.Size = new System.Drawing.Size(95, 23);
            this.ComprarVerTodoRefriSamsung3.TabIndex = 52;
            this.ComprarVerTodoRefriSamsung3.Text = "Comprar";
            this.ComprarVerTodoRefriSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoRefriSamsung2
            // 
            this.AgregarVerTodoRefriSamsung2.Location = new System.Drawing.Point(310, 1288);
            this.AgregarVerTodoRefriSamsung2.Name = "AgregarVerTodoRefriSamsung2";
            this.AgregarVerTodoRefriSamsung2.Size = new System.Drawing.Size(95, 23);
            this.AgregarVerTodoRefriSamsung2.TabIndex = 51;
            this.AgregarVerTodoRefriSamsung2.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriSamsung2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriSamsung2
            // 
            this.ComprarVerTodoRefriSamsung2.Location = new System.Drawing.Point(200, 1288);
            this.ComprarVerTodoRefriSamsung2.Name = "ComprarVerTodoRefriSamsung2";
            this.ComprarVerTodoRefriSamsung2.Size = new System.Drawing.Size(90, 23);
            this.ComprarVerTodoRefriSamsung2.TabIndex = 50;
            this.ComprarVerTodoRefriSamsung2.Text = "Comprar";
            this.ComprarVerTodoRefriSamsung2.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoRefriSamsung1
            // 
            this.AgregarVerTodoRefriSamsung1.Location = new System.Drawing.Point(310, 1077);
            this.AgregarVerTodoRefriSamsung1.Name = "AgregarVerTodoRefriSamsung1";
            this.AgregarVerTodoRefriSamsung1.Size = new System.Drawing.Size(98, 23);
            this.AgregarVerTodoRefriSamsung1.TabIndex = 49;
            this.AgregarVerTodoRefriSamsung1.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriSamsung1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriSamsung1
            // 
            this.ComprarVerTodoRefriSamsung1.Location = new System.Drawing.Point(192, 1077);
            this.ComprarVerTodoRefriSamsung1.Name = "ComprarVerTodoRefriSamsung1";
            this.ComprarVerTodoRefriSamsung1.Size = new System.Drawing.Size(98, 23);
            this.ComprarVerTodoRefriSamsung1.TabIndex = 48;
            this.ComprarVerTodoRefriSamsung1.Text = "Comprar";
            this.ComprarVerTodoRefriSamsung1.UseVisualStyleBackColor = true;
            this.ComprarVerTodoRefriSamsung1.Click += new System.EventHandler(this.button12_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Comercializadora.Properties.Resources.Refrigeradorsamsung3;
            this.pictureBox5.Location = new System.Drawing.Point(7, 1318);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(125, 210);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox5.TabIndex = 47;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Comercializadora.Properties.Resources.Refrigerador_Samsung2;
            this.pictureBox6.Location = new System.Drawing.Point(7, 1106);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(125, 206);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox6.TabIndex = 46;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Comercializadora.Properties.Resources.Refrigerador_Samsung;
            this.pictureBox7.Location = new System.Drawing.Point(14, 894);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(118, 206);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox7.TabIndex = 45;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Comercializadora.Properties.Resources.samsung;
            this.pictureBox8.Location = new System.Drawing.Point(284, 807);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(223, 71);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox8.TabIndex = 44;
            this.pictureBox8.TabStop = false;
            // 
            // AgregarVerTodoRefriMabe1
            // 
            this.AgregarVerTodoRefriMabe1.Location = new System.Drawing.Point(310, 1807);
            this.AgregarVerTodoRefriMabe1.Name = "AgregarVerTodoRefriMabe1";
            this.AgregarVerTodoRefriMabe1.Size = new System.Drawing.Size(110, 23);
            this.AgregarVerTodoRefriMabe1.TabIndex = 88;
            this.AgregarVerTodoRefriMabe1.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriMabe1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriMabe1
            // 
            this.ComprarVerTodoRefriMabe1.Location = new System.Drawing.Point(200, 1807);
            this.ComprarVerTodoRefriMabe1.Name = "ComprarVerTodoRefriMabe1";
            this.ComprarVerTodoRefriMabe1.Size = new System.Drawing.Size(95, 23);
            this.ComprarVerTodoRefriMabe1.TabIndex = 87;
            this.ComprarVerTodoRefriMabe1.Text = "Comprar";
            this.ComprarVerTodoRefriMabe1.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoRefriMabe2
            // 
            this.AgregarVerTodoRefriMabe2.Location = new System.Drawing.Point(318, 1995);
            this.AgregarVerTodoRefriMabe2.Name = "AgregarVerTodoRefriMabe2";
            this.AgregarVerTodoRefriMabe2.Size = new System.Drawing.Size(102, 23);
            this.AgregarVerTodoRefriMabe2.TabIndex = 86;
            this.AgregarVerTodoRefriMabe2.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriMabe2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriMabe2
            // 
            this.ComprarVerTodoRefriMabe2.Location = new System.Drawing.Point(204, 1997);
            this.ComprarVerTodoRefriMabe2.Name = "ComprarVerTodoRefriMabe2";
            this.ComprarVerTodoRefriMabe2.Size = new System.Drawing.Size(96, 23);
            this.ComprarVerTodoRefriMabe2.TabIndex = 85;
            this.ComprarVerTodoRefriMabe2.Text = "Comprar";
            this.ComprarVerTodoRefriMabe2.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoRefriMabe3
            // 
            this.AgregarVerTodoRefriMabe3.Location = new System.Drawing.Point(318, 2211);
            this.AgregarVerTodoRefriMabe3.Name = "AgregarVerTodoRefriMabe3";
            this.AgregarVerTodoRefriMabe3.Size = new System.Drawing.Size(102, 23);
            this.AgregarVerTodoRefriMabe3.TabIndex = 84;
            this.AgregarVerTodoRefriMabe3.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriMabe3.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriMabe3
            // 
            this.ComprarVerTodoRefriMabe3.Location = new System.Drawing.Point(198, 2211);
            this.ComprarVerTodoRefriMabe3.Name = "ComprarVerTodoRefriMabe3";
            this.ComprarVerTodoRefriMabe3.Size = new System.Drawing.Size(102, 23);
            this.ComprarVerTodoRefriMabe3.TabIndex = 83;
            this.ComprarVerTodoRefriMabe3.Text = "Comprar";
            this.ComprarVerTodoRefriMabe3.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(201, 1750);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(568, 52);
            this.label26.TabIndex = 82;
            this.label26.Text = resources.GetString("label26.Text");
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(195, 1720);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(69, 15);
            this.label27.TabIndex = 81;
            this.label27.Text = "$10,299.00";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(198, 1691);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(82, 15);
            this.label28.TabIndex = 80;
            this.label28.Text = "Marca: MABE";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(198, 1931);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(485, 52);
            this.label29.TabIndex = 79;
            this.label29.Text = resources.GetString("label29.Text");
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(195, 1907);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(65, 15);
            this.label30.TabIndex = 78;
            this.label30.Text = "$ 8,999.00";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(195, 1883);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 15);
            this.label31.TabIndex = 77;
            this.label31.Text = "Marca: MABE";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(194, 1849);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(300, 22);
            this.label32.TabIndex = 76;
            this.label32.Text = "REFRIGERADOR MABE 14 PIES";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(195, 2149);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(470, 52);
            this.label33.TabIndex = 75;
            this.label33.Text = resources.GetString("label33.Text");
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(195, 2125);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(69, 15);
            this.label34.TabIndex = 74;
            this.label34.Text = "$12,495.00";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(195, 2100);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(82, 15);
            this.label35.TabIndex = 73;
            this.label35.Text = "Marca: MABE";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(194, 2044);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(416, 44);
            this.label36.TabIndex = 72;
            this.label36.Text = "Refrigerador Whirlpool 17 pies cúbicos gris \r\nacero WT1756A";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(194, 1655);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(563, 22);
            this.label37.TabIndex = 71;
            this.label37.Text = "Mabe - Refrigerador Top Mount 14\" RME1436ZMFP0 - Negro";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Comercializadora.Properties.Resources.refrimabe3;
            this.pictureBox9.Location = new System.Drawing.Point(20, 2044);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(112, 190);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox9.TabIndex = 70;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Comercializadora.Properties.Resources.Refrimabe2;
            this.pictureBox10.Location = new System.Drawing.Point(20, 1849);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(112, 172);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox10.TabIndex = 69;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Comercializadora.Properties.Resources.RefriMabe;
            this.pictureBox11.Location = new System.Drawing.Point(20, 1655);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(112, 175);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox11.TabIndex = 68;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Comercializadora.Properties.Resources.Mabe;
            this.pictureBox12.Location = new System.Drawing.Point(284, 1565);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(231, 66);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox12.TabIndex = 67;
            this.pictureBox12.TabStop = false;
            // 
            // AgregarVerTodoRefriLg3
            // 
            this.AgregarVerTodoRefriLg3.Location = new System.Drawing.Point(328, 3022);
            this.AgregarVerTodoRefriLg3.Name = "AgregarVerTodoRefriLg3";
            this.AgregarVerTodoRefriLg3.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoRefriLg3.TabIndex = 110;
            this.AgregarVerTodoRefriLg3.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriLg3.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriLg3
            // 
            this.ComprarVerTodoRefriLg3.Location = new System.Drawing.Point(206, 3022);
            this.ComprarVerTodoRefriLg3.Name = "ComprarVerTodoRefriLg3";
            this.ComprarVerTodoRefriLg3.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoRefriLg3.TabIndex = 109;
            this.ComprarVerTodoRefriLg3.Text = "Comprar";
            this.ComprarVerTodoRefriLg3.UseVisualStyleBackColor = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(207, 2958);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(148, 39);
            this.label38.TabIndex = 108;
            this.label38.Text = "Puerta de enfriamiento\r\nCompresor inversor inteligente\r\nLuz LED";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label39.Location = new System.Drawing.Point(207, 2905);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(72, 15);
            this.label39.TabIndex = 107;
            this.label39.Text = "$ 35,990.00";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(206, 2878);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(64, 15);
            this.label40.TabIndex = 106;
            this.label40.Text = "Marca: LG";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label41.Location = new System.Drawing.Point(206, 2854);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(446, 22);
            this.label41.TabIndex = 105;
            this.label41.Text = "Refrigerador 15 Pies LG con Dispensador Plata";
            // 
            // AgregarVerTodoRefriLg2
            // 
            this.AgregarVerTodoRefriLg2.Location = new System.Drawing.Point(328, 2815);
            this.AgregarVerTodoRefriLg2.Name = "AgregarVerTodoRefriLg2";
            this.AgregarVerTodoRefriLg2.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoRefriLg2.TabIndex = 104;
            this.AgregarVerTodoRefriLg2.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriLg2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriLg2
            // 
            this.ComprarVerTodoRefriLg2.Location = new System.Drawing.Point(206, 2815);
            this.ComprarVerTodoRefriLg2.Name = "ComprarVerTodoRefriLg2";
            this.ComprarVerTodoRefriLg2.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoRefriLg2.TabIndex = 103;
            this.ComprarVerTodoRefriLg2.Text = "Comprar";
            this.ComprarVerTodoRefriLg2.UseVisualStyleBackColor = true;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(203, 2731);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(184, 39);
            this.label42.TabIndex = 102;
            this.label42.Text = "Función de diagnóstico inteligente\r\nDispensador y fábrica de hielos\r\nSistema de e" +
    "nfriamiento Multi Air Flow";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(206, 2685);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(72, 15);
            this.label43.TabIndex = 101;
            this.label43.Text = "$ 35,990.00";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(206, 2659);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(64, 15);
            this.label44.TabIndex = 100;
            this.label44.Text = "Marca: LG";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label45.Location = new System.Drawing.Point(206, 2627);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(462, 22);
            this.label45.TabIndex = 99;
            this.label45.Text = "Refrigerador 22 Pies LG con Despachador Negro";
            // 
            // AgregarVerTodoRefriLg1
            // 
            this.AgregarVerTodoRefriLg1.Location = new System.Drawing.Point(328, 2587);
            this.AgregarVerTodoRefriLg1.Name = "AgregarVerTodoRefriLg1";
            this.AgregarVerTodoRefriLg1.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoRefriLg1.TabIndex = 98;
            this.AgregarVerTodoRefriLg1.Text = "Agregar al carrito";
            this.AgregarVerTodoRefriLg1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoRefriLg1
            // 
            this.ComprarVerTodoRefriLg1.Location = new System.Drawing.Point(206, 2587);
            this.ComprarVerTodoRefriLg1.Name = "ComprarVerTodoRefriLg1";
            this.ComprarVerTodoRefriLg1.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoRefriLg1.TabIndex = 97;
            this.ComprarVerTodoRefriLg1.Text = "Comprar";
            this.ComprarVerTodoRefriLg1.UseVisualStyleBackColor = true;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(203, 2508);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(369, 39);
            this.label46.TabIndex = 96;
            this.label46.Text = "Recomendable para 7 personas en adelante\r\nMedidas sin paquete 174 X 75.6 X 90.1\r\n" +
    "Inversor inteligente ahorro de energía de hasta 32% vs modelo convencional";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(206, 2459);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(72, 15);
            this.label47.TabIndex = 95;
            this.label47.Text = "$ 15,990.00";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(206, 2434);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(64, 15);
            this.label48.TabIndex = 94;
            this.label48.Text = "Marca: LG";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(202, 2408);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(391, 22);
            this.label49.TabIndex = 93;
            this.label49.Text = "Refrigerador 22 Pies LG Acero Inoxidable";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Comercializadora.Properties.Resources.Lg;
            this.pictureBox13.Location = new System.Drawing.Point(284, 2252);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(293, 136);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox13.TabIndex = 92;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Comercializadora.Properties.Resources.RefriLg6;
            this.pictureBox14.Location = new System.Drawing.Point(17, 2854);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(117, 213);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox14.TabIndex = 91;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Comercializadora.Properties.Resources.RefriLg4;
            this.pictureBox15.Location = new System.Drawing.Point(17, 2627);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(117, 212);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox15.TabIndex = 90;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Comercializadora.Properties.Resources.RefriLG1;
            this.pictureBox16.Location = new System.Drawing.Point(16, 2408);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(118, 202);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox16.TabIndex = 89;
            this.pictureBox16.TabStop = false;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(-3, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 143;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // VerMarcasRefri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(863, 838);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.AgregarVerTodoRefriLg3);
            this.Controls.Add(this.ComprarVerTodoRefriLg3);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.AgregarVerTodoRefriLg2);
            this.Controls.Add(this.ComprarVerTodoRefriLg2);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.AgregarVerTodoRefriLg1);
            this.Controls.Add(this.ComprarVerTodoRefriLg1);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.AgregarVerTodoRefriMabe1);
            this.Controls.Add(this.ComprarVerTodoRefriMabe1);
            this.Controls.Add(this.AgregarVerTodoRefriMabe2);
            this.Controls.Add(this.ComprarVerTodoRefriMabe2);
            this.Controls.Add(this.AgregarVerTodoRefriMabe3);
            this.Controls.Add(this.ComprarVerTodoRefriMabe3);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.AgregarVerTodoRefriSamsung3);
            this.Controls.Add(this.ComprarVerTodoRefriSamsung3);
            this.Controls.Add(this.AgregarVerTodoRefriSamsung2);
            this.Controls.Add(this.ComprarVerTodoRefriSamsung2);
            this.Controls.Add(this.AgregarVerTodoRefriSamsung1);
            this.Controls.Add(this.ComprarVerTodoRefriSamsung1);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.AgregarVerTodoRefriWhirlpool3);
            this.Controls.Add(this.ComprarVerTodoRefriWhirlpool3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.AgregarVerTodoRefriWhirlpool2);
            this.Controls.Add(this.ComprarVerTodoRefriWhirlpool2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarVerTodoRefriWhirlpool1);
            this.Controls.Add(this.ComprarVerTodoRefriWhirlpool1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "VerMarcasRefri";
            this.Text = "VerMarcasRefri";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AgregarVerTodoRefriWhirlpool3;
        private System.Windows.Forms.Button ComprarVerTodoRefriWhirlpool3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button AgregarVerTodoRefriWhirlpool2;
        private System.Windows.Forms.Button ComprarVerTodoRefriWhirlpool2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarVerTodoRefriWhirlpool1;
        private System.Windows.Forms.Button ComprarVerTodoRefriWhirlpool1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button AgregarVerTodoRefriSamsung3;
        private System.Windows.Forms.Button ComprarVerTodoRefriSamsung3;
        private System.Windows.Forms.Button AgregarVerTodoRefriSamsung2;
        private System.Windows.Forms.Button ComprarVerTodoRefriSamsung2;
        private System.Windows.Forms.Button AgregarVerTodoRefriSamsung1;
        private System.Windows.Forms.Button ComprarVerTodoRefriSamsung1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Button AgregarVerTodoRefriMabe1;
        private System.Windows.Forms.Button ComprarVerTodoRefriMabe1;
        private System.Windows.Forms.Button AgregarVerTodoRefriMabe2;
        private System.Windows.Forms.Button ComprarVerTodoRefriMabe2;
        private System.Windows.Forms.Button AgregarVerTodoRefriMabe3;
        private System.Windows.Forms.Button ComprarVerTodoRefriMabe3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Button AgregarVerTodoRefriLg3;
        private System.Windows.Forms.Button ComprarVerTodoRefriLg3;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button AgregarVerTodoRefriLg2;
        private System.Windows.Forms.Button ComprarVerTodoRefriLg2;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button AgregarVerTodoRefriLg1;
        private System.Windows.Forms.Button ComprarVerTodoRefriLg1;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}