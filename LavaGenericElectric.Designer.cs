﻿namespace Comercializadora
{
    partial class LavaGenericElectric
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ComprarLavaWhirlpool1 = new System.Windows.Forms.Button();
            this.AgregarLavaWhirlpool1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ComprarLavaWhirlpool2 = new System.Windows.Forms.Button();
            this.AgregarLavaWhirlpool2 = new System.Windows.Forms.Button();
            this.AgregarLavaWhirlpool3 = new System.Windows.Forms.Button();
            this.ComprarLavaWhirlpool3 = new System.Windows.Forms.Button();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(204, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(378, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Lavadora Generic Electric 21 Kg Blanca";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(208, 189);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Marca: GENERIC ELECTRIC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(208, 216);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "$ 8,490.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(205, 246);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(337, 39);
            this.label4.TabIndex = 7;
            this.label4.Text = "Tapa de cristal templado con seguro\r\nPanel con 4 perillas\r\nDespachador de blanque" +
    "ador en cubierta y de suavizante en agitador";
            // 
            // ComprarLavaWhirlpool1
            // 
            this.ComprarLavaWhirlpool1.Location = new System.Drawing.Point(208, 350);
            this.ComprarLavaWhirlpool1.Name = "ComprarLavaWhirlpool1";
            this.ComprarLavaWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.ComprarLavaWhirlpool1.TabIndex = 10;
            this.ComprarLavaWhirlpool1.Text = "Comprar";
            this.ComprarLavaWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // AgregarLavaWhirlpool1
            // 
            this.AgregarLavaWhirlpool1.Location = new System.Drawing.Point(348, 350);
            this.AgregarLavaWhirlpool1.Name = "AgregarLavaWhirlpool1";
            this.AgregarLavaWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.AgregarLavaWhirlpool1.TabIndex = 11;
            this.AgregarLavaWhirlpool1.Text = "Agregar al carrito";
            this.AgregarLavaWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(207, 396);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(418, 22);
            this.label5.TabIndex = 12;
            this.label5.Text = "Lavadora GENERIC ELECTRIC 20 Kg Blanca";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(208, 430);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 15);
            this.label6.TabIndex = 13;
            this.label6.Text = "Marca: GENERIC ELECTRIC";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(211, 461);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "$ 7,490.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(205, 508);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(224, 52);
            this.label8.TabIndex = 15;
            this.label8.Text = "Recomendable para 5 o más personas\r\nSistema de lavado por agitador\r\nLavado Expres" +
    "s en 20 minutos\r\nAhorra agua hasta 76% con aqua saver green";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.LavaGeneric3;
            this.pictureBox4.Location = new System.Drawing.Point(12, 616);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(152, 207);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.LavaGeneric2_1;
            this.pictureBox3.Location = new System.Drawing.Point(12, 396);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(151, 214);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.LavaGeneric11;
            this.pictureBox2.Location = new System.Drawing.Point(12, 144);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(152, 229);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.Generic_Electronic;
            this.pictureBox1.Location = new System.Drawing.Point(173, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(440, 106);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(208, 616);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(305, 22);
            this.label9.TabIndex = 16;
            this.label9.Text = "Lavadora Daewoo 19 Kg Blanca";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(211, 647);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Marca: GENERIC ELECTRIC";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(211, 674);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 15);
            this.label11.TabIndex = 18;
            this.label11.Text = "$6,190.00";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(208, 713);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(253, 52);
            this.label12.TabIndex = 19;
            this.label12.Text = "Tecnología de lavado Air Bubbler 4D\r\nFiltro atrapapelusa\r\nCascada Dynamic Waterfa" +
    "ll\r\nCanasta de acero inoxidable con grabado Star Tubs";
            // 
            // ComprarLavaWhirlpool2
            // 
            this.ComprarLavaWhirlpool2.Location = new System.Drawing.Point(208, 577);
            this.ComprarLavaWhirlpool2.Name = "ComprarLavaWhirlpool2";
            this.ComprarLavaWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.ComprarLavaWhirlpool2.TabIndex = 20;
            this.ComprarLavaWhirlpool2.Text = "Comprar";
            this.ComprarLavaWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // AgregarLavaWhirlpool2
            // 
            this.AgregarLavaWhirlpool2.Location = new System.Drawing.Point(348, 577);
            this.AgregarLavaWhirlpool2.Name = "AgregarLavaWhirlpool2";
            this.AgregarLavaWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.AgregarLavaWhirlpool2.TabIndex = 21;
            this.AgregarLavaWhirlpool2.Text = "Agregar al carrito";
            this.AgregarLavaWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // AgregarLavaWhirlpool3
            // 
            this.AgregarLavaWhirlpool3.Location = new System.Drawing.Point(348, 800);
            this.AgregarLavaWhirlpool3.Name = "AgregarLavaWhirlpool3";
            this.AgregarLavaWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.AgregarLavaWhirlpool3.TabIndex = 22;
            this.AgregarLavaWhirlpool3.Text = "Agregar al carrito";
            this.AgregarLavaWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // ComprarLavaWhirlpool3
            // 
            this.ComprarLavaWhirlpool3.Location = new System.Drawing.Point(208, 795);
            this.ComprarLavaWhirlpool3.Name = "ComprarLavaWhirlpool3";
            this.ComprarLavaWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.ComprarLavaWhirlpool3.TabIndex = 23;
            this.ComprarLavaWhirlpool3.Text = "Comprar";
            this.ComprarLavaWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(-1, -5);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 142;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // LavaGenericElectric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 835);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.ComprarLavaWhirlpool3);
            this.Controls.Add(this.AgregarLavaWhirlpool3);
            this.Controls.Add(this.AgregarLavaWhirlpool2);
            this.Controls.Add(this.ComprarLavaWhirlpool2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarLavaWhirlpool1);
            this.Controls.Add(this.ComprarLavaWhirlpool1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "LavaGenericElectric";
            this.Text = "LavaGenericElectric";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ComprarLavaWhirlpool1;
        private System.Windows.Forms.Button AgregarLavaWhirlpool1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button ComprarLavaWhirlpool2;
        private System.Windows.Forms.Button AgregarLavaWhirlpool2;
        private System.Windows.Forms.Button AgregarLavaWhirlpool3;
        private System.Windows.Forms.Button ComprarLavaWhirlpool3;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}