﻿namespace Comercializadora
{
    partial class LavaWhirlpool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LavaWhirlpool));
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.AgregarLavaWhirlpool2 = new System.Windows.Forms.Button();
            this.ComprarLavaWhirlpool2 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarLavaWhirlpool1 = new System.Windows.Forms.Button();
            this.ComprarLavaWhirlpool1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ComprarLavaWhirlpool3 = new System.Windows.Forms.Button();
            this.AgregarLavaWhirlpool3 = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(215, 731);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(443, 117);
            this.label12.TabIndex = 36;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(215, 704);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 35;
            this.label11.Text = "$ 9,790.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(215, 679);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 15);
            this.label10.TabIndex = 34;
            this.label10.Text = "Marca: WHIRLPOOL";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(214, 646);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(315, 22);
            this.label9.TabIndex = 33;
            this.label9.Text = "Lavadora Whirlpool 20 kg Blanca";
            // 
            // AgregarLavaWhirlpool2
            // 
            this.AgregarLavaWhirlpool2.Location = new System.Drawing.Point(343, 604);
            this.AgregarLavaWhirlpool2.Name = "AgregarLavaWhirlpool2";
            this.AgregarLavaWhirlpool2.Size = new System.Drawing.Size(103, 23);
            this.AgregarLavaWhirlpool2.TabIndex = 32;
            this.AgregarLavaWhirlpool2.Text = "Agregar al carrito";
            this.AgregarLavaWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // ComprarLavaWhirlpool2
            // 
            this.ComprarLavaWhirlpool2.Location = new System.Drawing.Point(218, 604);
            this.ComprarLavaWhirlpool2.Name = "ComprarLavaWhirlpool2";
            this.ComprarLavaWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.ComprarLavaWhirlpool2.TabIndex = 31;
            this.ComprarLavaWhirlpool2.Text = "Comprar";
            this.ComprarLavaWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(215, 489);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(545, 91);
            this.label8.TabIndex = 30;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(215, 474);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 15);
            this.label7.TabIndex = 29;
            this.label7.Text = "$ 7,990.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(215, 447);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 15);
            this.label6.TabIndex = 28;
            this.label6.Text = "Marca: WHIRLPOOL ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(214, 416);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(373, 22);
            this.label5.TabIndex = 27;
            this.label5.Text = "Lavadora Whirlpool Xpert 18 Kg Blanca";
            // 
            // AgregarLavaWhirlpool1
            // 
            this.AgregarLavaWhirlpool1.Location = new System.Drawing.Point(343, 358);
            this.AgregarLavaWhirlpool1.Name = "AgregarLavaWhirlpool1";
            this.AgregarLavaWhirlpool1.Size = new System.Drawing.Size(103, 23);
            this.AgregarLavaWhirlpool1.TabIndex = 26;
            this.AgregarLavaWhirlpool1.Text = "Agregar al carrito";
            this.AgregarLavaWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // ComprarLavaWhirlpool1
            // 
            this.ComprarLavaWhirlpool1.Location = new System.Drawing.Point(218, 358);
            this.ComprarLavaWhirlpool1.Name = "ComprarLavaWhirlpool1";
            this.ComprarLavaWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.ComprarLavaWhirlpool1.TabIndex = 25;
            this.ComprarLavaWhirlpool1.Text = "Comprar";
            this.ComprarLavaWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(215, 276);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(350, 39);
            this.label4.TabIndex = 24;
            this.label4.Text = "Ideal para 5 o más personas\r\n12 Ciclos para todo tipo de ropa y cuidado de ropa\r\n" +
    "Sistema de lavado Xpert Sistema que remueve las manchas más difíciles\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(215, 229);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 15);
            this.label3.TabIndex = 23;
            this.label3.Text = "$ 7,390.00}";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(215, 199);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 15);
            this.label2.TabIndex = 22;
            this.label2.Text = "Marca: WHIRLPOOL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(214, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(318, 22);
            this.label1.TabIndex = 21;
            this.label1.Text = "Lavadora Whirlpool 18 Kg Blanca";
            // 
            // ComprarLavaWhirlpool3
            // 
            this.ComprarLavaWhirlpool3.Location = new System.Drawing.Point(218, 864);
            this.ComprarLavaWhirlpool3.Name = "ComprarLavaWhirlpool3";
            this.ComprarLavaWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.ComprarLavaWhirlpool3.TabIndex = 37;
            this.ComprarLavaWhirlpool3.Text = "Comprar";
            this.ComprarLavaWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // AgregarLavaWhirlpool3
            // 
            this.AgregarLavaWhirlpool3.Location = new System.Drawing.Point(343, 864);
            this.AgregarLavaWhirlpool3.Name = "AgregarLavaWhirlpool3";
            this.AgregarLavaWhirlpool3.Size = new System.Drawing.Size(103, 23);
            this.AgregarLavaWhirlpool3.TabIndex = 38;
            this.AgregarLavaWhirlpool3.Text = "Agregar al carrito";
            this.AgregarLavaWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.Lavawhirlpool3;
            this.pictureBox4.Location = new System.Drawing.Point(16, 646);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(163, 225);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.Lavawhirlpool2;
            this.pictureBox3.Location = new System.Drawing.Point(16, 413);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(163, 214);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.Lavawhirlpool1;
            this.pictureBox2.Location = new System.Drawing.Point(16, 162);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(163, 219);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.whirlpool;
            this.pictureBox1.Location = new System.Drawing.Point(273, 34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(237, 99);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(-2, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 142;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // LavaWhirlpool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(885, 832);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.AgregarLavaWhirlpool3);
            this.Controls.Add(this.ComprarLavaWhirlpool3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.AgregarLavaWhirlpool2);
            this.Controls.Add(this.ComprarLavaWhirlpool2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarLavaWhirlpool1);
            this.Controls.Add(this.ComprarLavaWhirlpool1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "LavaWhirlpool";
            this.Text = "LavaWhirlpool";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button AgregarLavaWhirlpool2;
        private System.Windows.Forms.Button ComprarLavaWhirlpool2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarLavaWhirlpool1;
        private System.Windows.Forms.Button ComprarLavaWhirlpool1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ComprarLavaWhirlpool3;
        private System.Windows.Forms.Button AgregarLavaWhirlpool3;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}