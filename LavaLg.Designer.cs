﻿namespace Comercializadora
{
    partial class LavaLg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LavaLg));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ComprarLavaLg1 = new System.Windows.Forms.Button();
            this.AgregarLavaLg1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ComprarLavaLg2 = new System.Windows.Forms.Button();
            this.AgregarLavaLg2 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ComprarLavaLg3 = new System.Windows.Forms.Button();
            this.AgregarLavaLg3 = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(222, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Lavadora LG 22 kg Blanca";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(223, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Marca: LG";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(223, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "$ 11,590.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(223, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(355, 91);
            this.label4.TabIndex = 8;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // ComprarLavaLg1
            // 
            this.ComprarLavaLg1.Location = new System.Drawing.Point(226, 353);
            this.ComprarLavaLg1.Name = "ComprarLavaLg1";
            this.ComprarLavaLg1.Size = new System.Drawing.Size(103, 23);
            this.ComprarLavaLg1.TabIndex = 9;
            this.ComprarLavaLg1.Text = "Comprar";
            this.ComprarLavaLg1.UseVisualStyleBackColor = true;
            // 
            // AgregarLavaLg1
            // 
            this.AgregarLavaLg1.Location = new System.Drawing.Point(344, 354);
            this.AgregarLavaLg1.Name = "AgregarLavaLg1";
            this.AgregarLavaLg1.Size = new System.Drawing.Size(103, 23);
            this.AgregarLavaLg1.TabIndex = 10;
            this.AgregarLavaLg1.Text = "Agregar al carrito";
            this.AgregarLavaLg1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(222, 398);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(322, 22);
            this.label5.TabIndex = 11;
            this.label5.Text = "Lavadora LG Smart Inverter 18 kg";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(223, 432);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "Marca: LG";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(223, 456);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "$ 11,590.00";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(223, 483);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(422, 78);
            this.label8.TabIndex = 14;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // ComprarLavaLg2
            // 
            this.ComprarLavaLg2.Location = new System.Drawing.Point(226, 574);
            this.ComprarLavaLg2.Name = "ComprarLavaLg2";
            this.ComprarLavaLg2.Size = new System.Drawing.Size(103, 23);
            this.ComprarLavaLg2.TabIndex = 15;
            this.ComprarLavaLg2.Text = "Comprar";
            this.ComprarLavaLg2.UseVisualStyleBackColor = true;
            // 
            // AgregarLavaLg2
            // 
            this.AgregarLavaLg2.Location = new System.Drawing.Point(344, 574);
            this.AgregarLavaLg2.Name = "AgregarLavaLg2";
            this.AgregarLavaLg2.Size = new System.Drawing.Size(103, 23);
            this.AgregarLavaLg2.TabIndex = 16;
            this.AgregarLavaLg2.Text = "Agregar al carrito";
            this.AgregarLavaLg2.UseVisualStyleBackColor = true;
            this.AgregarLavaLg2.Click += new System.EventHandler(this.button4_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(222, 618);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(308, 22);
            this.label9.TabIndex = 17;
            this.label9.Text = "Lavadora LG 22 Kg Plata Deluxe";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(223, 651);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 15);
            this.label10.TabIndex = 18;
            this.label10.Text = "Marca: LG";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(223, 676);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 15);
            this.label11.TabIndex = 19;
            this.label11.Text = "$ 13,990.00}";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(223, 714);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(212, 52);
            this.label12.TabIndex = 20;
            this.label12.Text = "Lavadora con 8 ciclos de lavado\r\nPuerta de vidrio templado\r\nAcabado de la canasta" +
    " de acero inoxidable\r\nProgramación Automática";
            // 
            // ComprarLavaLg3
            // 
            this.ComprarLavaLg3.Location = new System.Drawing.Point(225, 811);
            this.ComprarLavaLg3.Name = "ComprarLavaLg3";
            this.ComprarLavaLg3.Size = new System.Drawing.Size(104, 23);
            this.ComprarLavaLg3.TabIndex = 21;
            this.ComprarLavaLg3.Text = "Comprar";
            this.ComprarLavaLg3.UseVisualStyleBackColor = true;
            // 
            // AgregarLavaLg3
            // 
            this.AgregarLavaLg3.Location = new System.Drawing.Point(344, 811);
            this.AgregarLavaLg3.Name = "AgregarLavaLg3";
            this.AgregarLavaLg3.Size = new System.Drawing.Size(103, 23);
            this.AgregarLavaLg3.TabIndex = 22;
            this.AgregarLavaLg3.Text = "Agregar al carrito";
            this.AgregarLavaLg3.UseVisualStyleBackColor = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.LavaLg3;
            this.pictureBox4.Location = new System.Drawing.Point(12, 618);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(158, 216);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.LavaLg2;
            this.pictureBox3.Location = new System.Drawing.Point(12, 401);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(158, 196);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.LavaLg1;
            this.pictureBox2.Location = new System.Drawing.Point(12, 144);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(157, 233);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.Lg;
            this.pictureBox1.Location = new System.Drawing.Point(245, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(286, 134);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(0, 2);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 142;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // LavaLg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(725, 827);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.AgregarLavaLg3);
            this.Controls.Add(this.ComprarLavaLg3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.AgregarLavaLg2);
            this.Controls.Add(this.ComprarLavaLg2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarLavaLg1);
            this.Controls.Add(this.ComprarLavaLg1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "LavaLg";
            this.Text = "LavaLg";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ComprarLavaLg1;
        private System.Windows.Forms.Button AgregarLavaLg1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button ComprarLavaLg2;
        private System.Windows.Forms.Button AgregarLavaLg2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button ComprarLavaLg3;
        private System.Windows.Forms.Button AgregarLavaLg3;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}